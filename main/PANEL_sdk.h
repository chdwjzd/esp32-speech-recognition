#ifndef _PANEL_SDK_H_
#define _PANEL_SDK_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "board.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "mint_sdk.h"

#define u8 unsigned char
#define u16 unsigned short
#define u32 unsigned int
#define u64 unsigned long long int

#define data_size 512

typedef enum
{
	Recognition_GETWAY = 0x00,		//语音识别网关
	Recognition_PANEL,				//普通语音识别面板，无联网及触屏
	Recognition_TOUCH_PANEL,		//带触屏的语音识别面板，无联网
	Online_Recognition_PANEL,		//在线语音识别面板，无触屏
	Online_Recognition_TOUCH_PANEL, //在线带触屏语音识别面板
	Unknown_model = 0xff,			//未知设备类型
} PANEL_model_list;					//设备小类定义

typedef enum
{
	Offline_Recognition = 0x00, //离线识别模式
	Online_Recognition,			//在线识别模式
	DUAL_Recognition,			//双线识别模式
} PANEL_fuction;				//面板主要识别功能

typedef enum
{
	Normal_Mode = 0x00, //普通模式，采集音频识别唤醒词
	WakeUp_Mode,		//唤醒模式，采集音频识别命令词
	Mute_Mode,			//静音模式，不采集音频
	OTA_Mode,			//OTA升级模式，不采集音频
} PANEL_LocalState;		//PANEL的本地功能状态

typedef enum
{
	Offline = 0x00,		//离线状态
	Online,				//在线状态，未激活设备或不能连接服务器
	Activation,			//在线状态且设备已经激活
	Activation_Expired, //在线状态但设备激活过期
} PANEL_NetState;		//PANEL的在线功能状态

typedef enum
{
	nihao = 0x00, //你好
	zai,		  //在
	zsh,		  //早上好
	haode,		  //好的
	zxcg,		  //执行成功
	zxsb,		  //执行失败
	zxsbqjc,	  //执行失败，请检查后重试
	wmtd,		  //我没听懂
	wmtqc,		  //我没听清楚，请再说一次
	mygsb,			//当前房价没有设备
} play_list;	  //PANEL的在线功能状态

typedef struct
{
	u8 OnlineIP[4];		 //在线识别服务的连接IP
	u16 OnlinePort;		 //在线识别服务的连接端口
	char OnlineName[20]; //在线识别服务的用户名
	char OnlinePWD[20];	 //在线识别服务的密码
} PANEL_OnlineInfo;		 //在线服务信息，没在线识别服务全部填0

typedef struct
{
	u8 MAC_addres[6];			  //PANEL工厂MAC地址
	u8 Hardware_ver;			  //语音识别设备的硬件版本
	char Software_ver[32];		  //语音识别设备的软件版本
	u8 Main_type;				  //设备大类
	PANEL_model_list PANEL_model; //设备小类定
	u8 WN_char[30];				  //唤醒词的拼音
	PANEL_fuction fuction;		  //语音识别设备的主要功能
	u8 boot_voice_lev;			  //设备启动默认音量
	u16 free_RAM;				  //剩余的RAM大小
	u8 MN_Num;					  //命令词条数
	u8 free_MN_Num;				  //空余命令词条数，不能设置填255
	u8 Button_Num;				  //设备按键数量，除了Reset按键外的所有按键
	u8 BOOT_Partition;			  //当前启动分区
	u8 Selftest;				  //自检状态，正常为0，否则为错误号
} PANEL_SYSInfo;				  //回报语音识别设备的系统信息。  获取信息：PANEL_pkt.PANEL_pkt_head.MINT_CMD_getdevinfo（data留空）
								  //PANEL返回信息：PANEL_pkt.PANEL_pkt_head.MINT_CMD_getdevinfo

typedef struct
{
	char PANEL_name[25]; //设备名称
	u8 PANEL_room[2];	 //设备区域
	u32 PANEL_did;		 //设备did
	u8 Voice_Lev;		 //设置的音量
	u8 Room_Lev;
	bool Mute;					  //设备启动是否静音
	PANEL_OnlineInfo OnlineInfo;  //在线服务信息，没在线识别服务全部填0
} PANEL_SetInfo, *pPANEL_SetInfo; //通知及回报语音识别设备可配置信息。    设置：PANEL_pkt.PANEL_pkt_head.MINT_CMD_setinfo;
								  //读取：PANEL_pkt.PANEL_pkt_head.MINT_CMD_get_setinfo（data留空）
								  //PANEL返回信息：PANEL_pkt.PANEL_pkt_head.MINT_CMD_get_setinfo；

typedef struct
{
	u8 PANEL_room[2];					  //设备区域
	u32 PANEL_did;						  //设备did
	u8 Recognition_Num;					  //命令识别序号
	u8 Matching_Rate;					  //命令识别匹配率
	u8 Use_Rate;						  //命令使用率
	u8 Speech_Lev;						  //语音声音大小
	u8 Noice_Lev;						  //背景噪音等级
	u8 Room_Lev;		//房间等级
} PANEL_Recognition, *pPANEL_Recognition; //PANEL回报语音识别结果。    PANEL_pkt.PANEL_pkt_head.MINT_CMD_RecognitionMsg

typedef struct
{
	u8 Output_Format; //0:wav;1:mp3;2;acc
	u32 Output_Len;	  //输出的音频长度，单位：byte
	u8 Sendway;		  //输出方式；0:SPI;1:UART;2:LAN
} PANEL_OutVoice;	  //PANEL回报采集的语音输出方式。 PANEL_pkt.PANEL_pkt_head.MINT_CMD_SendSpeech

typedef struct
{
	play_list PlayNum;						//需要播放的音频序号。255：不需要播放；254：播放SPI传输的音频；253：播放LAN传输的音频。传输的音频格式：WAV,16000Hz,16Bit
	bool Need_SecInterctive;				//是否需要二次采样
} PANEL_HandleResult, *pPANEL_HandleResult; //通知执行结果。    PANEL_pkt.PANEL_pkt_head.MINT_CMD_HandleResult

typedef struct
{
	u8 ButtonNum;		//触发的按键序号
	u8 ButtonState; 	//按键状态
	u8 PANEL_room;		//设备区域
	u32 PANEL_did;		//设备did
} PANEL_ButtonInfo; 	//PANEL回报按键触发信息。    PANEL_pkt.PANEL_pkt_head.MINT_CMD_key

typedef struct
{
	bool StartOTA;					//是否开始OTA
	u32 OTA_Len;					//OTA数据长度
	u8 Sendway;						//传输方式；0:UART;1:SPI;2:LAN
} PANEL_StartOTA, *pPANEL_StartOTA; //通知开始OTA;通知： PANEL_pkt.PANEL_pkt_head.MINT_CMD_update
									//PANEL回报： PANEL_pkt.PANEL_pkt_head.MINT_CMD_update		StartOTA:指示是否准备好，OTA_Len及Sendway留空

// typedef struct
// {
// 	u8 ReadyOTA;					//是否可以OTA，可以为0，否则为错误号
// }PANEL_ReadyOTA;					//回报OTA准备完毕

typedef struct
{
	short OTA_State;			//OTA状态。0：成功；否则为错误号
	u8 OTA_SHA256[32];			//OTA分区的SHA256数值
	u8 Next_BOOT_Partition;		//下一个启动的分区
} PANEL_EndOTA, *pPANEL_EndOTA; //OTA结束回报信息。 回报： PANEL_pkt.PANEL_pkt_head.MINT_CMD_updateState
								//OTA成功：控制重启： PANEL_pkt.PANEL_pkt_head.MINT_CMD_reboot

typedef struct
{
	PANEL_LocalState LocalState;			  //PANEL的本地功能状态
	PANEL_NetState NetState;				  //PANEL的在线功能状态
} PANEL_FunctionState, *pPANEL_FunctionState; //获取PANEL功能状态。   获取：PANEL_pkt.PANEL_pkt_head.MINT_CMD_FunctionState（data留空）
											  //返回：PANEL_pkt.PANEL_pkt_head.MINT_CMD_FunctionState

typedef struct
{
	u32 PANEL_did;
	u8 sourceMAC[6];
	//    u8 sourceRoom;
} PANEL_SOURCEID;

typedef struct
{
	char fixed[9];			 	//默认头 'm' 'i' 'n' 't' 'P' 'A' 'N' 'E' 'L' ,初始化的时候需要自己赋予
	PANEL_SOURCEID sourceid; 	//发送的设备信息？
	MINT_CMD cmd;				//动作指令
	u16 dataLen; 				//data的长度
} PANEL_pkt_head;

typedef struct
{
	PANEL_pkt_head head; 	//数据包头
	u8 data[data_size];	 	//数据包内容
	u16 allsum;				//数据包校验和
} PANEL_pkt, *pPANEL_pkt; 	//



////////////////////////////////////////////////////////////
QueueHandle_t sdk_send_data;
QueueHandle_t sdk_rec_data;

typedef struct Send_msg
{
	u8 send_task;
	u8 sendlen;
	char *buf;
} Send_msg_T, *pSend_msg;
;

typedef struct rec_msg
{
	char *buf;
	u16 rec_len;
} rec_msg_T;

TaskHandle_t sendmsg_task_Handler;
TaskHandle_t recmsg_task_Handler;

void sendmsg_task(void *arg);
void recmsg_task(void *arg);

#endif
