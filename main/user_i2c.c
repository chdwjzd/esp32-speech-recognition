#include "user_i2c.h"
#include "driver/i2c.h"
#include "includes.h"

esp_err_t i2c_master_read_slave(uint8_t Address)
{
    uint8_t data_rd[1];

    uint8_t *p = data_rd;

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, 0b00100000, 0x1);
    i2c_master_write_byte(cmd, Address, 0x1);
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, 0b00100001, 0x1);
    i2c_master_read_byte(cmd,p,0x1);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    ESP_LOGI(TAG, "I2C read: %d; Address: %d;	err: %d",data_rd[0],Address,ret);
    return ret;
}


esp_err_t i2c_master_write_slave(uint8_t Address,uint8_t data)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, 0b00100000, 0x1);
    i2c_master_write_byte(cmd, Address, 0x1);
    i2c_master_write_byte(cmd, data , 0x1);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    ESP_LOGI(TAG, "I2C write: %d; Address: %d;	err: %d",data,Address,ret);
    return ret;
}

