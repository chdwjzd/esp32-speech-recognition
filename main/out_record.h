#ifndef _OUT_RECORD_H_
#define _OUT_RECORD_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "board.h"

TaskHandle_t outrecordTask_Handler;
#define RECORD_TASK_PRIO	12

QueueHandle_t record_size;

QueueHandle_t voice_cache_size;

typedef struct voice_cache_msg
{
	char *buf;
	int cache_size;
}voice_cache_msg_T;

void out_record(void *arg);


#endif
