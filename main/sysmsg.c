#include "sysmsg.h"
#include "driver/uart.h"
#include "includes.h"
#include "esp_ota_ops.h"

#include "ota.h"

bool contrast_SHA256(uint8_t sha_256_0[32], uint8_t sha_256_1[32])
{
	for (int i = 0; i < 32; i++)
	{
		if (sha_256_0[i] != sha_256_1[i])
			return false;
	}
	return true;
}

void updata_sha256(const esp_partition_t *updata_sha256_partition)
{
	const esp_partition_t *sha256_partition = NULL;
	uint8_t sha_256[32] = {0};
	esp_err_t err;
	int SHA256_offest = 0;
	//	char partiton_name[17] = {0};
	printf("updata sha256 partiton addr: 0x%08x; size: %d; label: %s\n", updata_sha256_partition->address, updata_sha256_partition->size, updata_sha256_partition->label);

	printf("FIND SHAVALUE partition\r\n");
	sha256_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
												ESP_PARTITION_SUBTYPE_DATA_FAT, SHAVALUE_PARTITION_NAME);
	if (sha256_partition != NULL)
	{
		printf("SHAVALUE partiton addr: 0x%08x; size: %d; label: %s\n", sha256_partition->address, sha256_partition->size, sha256_partition->label);
	}
	else
	{
		printf("SHAVALUE Partition error: can't find partition name: %s\n", SHAVALUE_PARTITION_NAME);
	}

	if (memcmp(updata_sha256_partition->label, "ota_0", strlen(updata_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0;
	}
	else if (memcmp(updata_sha256_partition->label, "ota_1", strlen(updata_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x100;
	}
	else if (memcmp(updata_sha256_partition->label, "SYSMSG", strlen(updata_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x200;
	}
	else if (memcmp(updata_sha256_partition->label, "SETMSG", strlen(updata_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x300;
	}
	else
	{
		printf("updata sha256_partition name error\r\n");
	}

	esp_partition_get_sha256(updata_sha256_partition, sha_256);

	printf("updata sha256 label: %s, SHA256_offest: 0x%04x\r\n", updata_sha256_partition->label, SHA256_offest);

	print_sha256(sha_256, "SHA-256 for the updata sha256 partition table: ");

	char *flash_read_sha256 = malloc(sha256_partition->size);

	esp_partition_read(sha256_partition, 0, flash_read_sha256, sha256_partition->size);

	err = esp_partition_erase_range(sha256_partition, 0, sha256_partition->size);
	if (err != ESP_OK)
		printf("sha256 partition erase error,range %s\r\n", esp_err_to_name(err));

	memcpy(flash_read_sha256 + SHA256_offest, sha_256, 32);

	esp_partition_write(sha256_partition, 0, flash_read_sha256, sha256_partition->size);

	esp_partition_read(sha256_partition, SHA256_offest, sha_256, 32);
	print_sha256(sha_256, "Read SHA-256 for the updata sha256 partition table: ");

	free(flash_read_sha256);
}

bool check_sha256(const esp_partition_t *cheak_sha256_partition)
{
	const esp_partition_t *sha256_partition = NULL;
	uint8_t sha_256[32] = {0};
	uint8_t read_sha_256[32] = {0};
	//	esp_err_t err;
	int SHA256_offest = 0;
	//	char partiton_name[17] = {0};
	printf("check sha256 partiton addr: 0x%08x; size: %d; label: %s\n", cheak_sha256_partition->address, cheak_sha256_partition->size, cheak_sha256_partition->label);

	printf("FIND SHAVALUE partition\r\n");
	sha256_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
												ESP_PARTITION_SUBTYPE_DATA_FAT, SHAVALUE_PARTITION_NAME);
	if (sha256_partition != NULL)
	{
		printf("SHAVALUE partiton addr: 0x%08x; size: %d; label: %s\n", sha256_partition->address, sha256_partition->size, sha256_partition->label);
	}
	else
	{
		printf("SHAVALUE Partition error: can't find partition name: %s\n", SHAVALUE_PARTITION_NAME);
	}

	if (memcmp(cheak_sha256_partition->label, "ota_0", strlen(cheak_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0;
	}
	else if (memcmp(cheak_sha256_partition->label, "ota_1", strlen(cheak_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x100;
	}
	else if (memcmp(cheak_sha256_partition->label, "SYSMSG", strlen(cheak_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x200;
	}
	else if (memcmp(cheak_sha256_partition->label, "SETMSG", strlen(cheak_sha256_partition->label)) == 0)
	{
		SHA256_offest = 0x300;
	}
	else
	{
		printf("check sha256_partition name error; label: %s\r\n", cheak_sha256_partition->label);
	}

	esp_partition_get_sha256(cheak_sha256_partition, sha_256);
	print_sha256(sha_256, "check SHA-256 for the partition table: ");

	esp_partition_read(sha256_partition, SHA256_offest, read_sha_256, 32);
	print_sha256(read_sha_256, "Read SHA-256 for the partition table: ");

	//////////////////////////////////////////////////          测试用          ///////////////////////////////
	//    esp_partition_erase_range(cheak_sha256_partition,0,cheak_sha256_partition->size);
	//    esp_partition_write(cheak_sha256_partition, 0x500, sha_256, 32);
	//    printf("rewrite \r\n");
	////////////////////////////////////////////////////////////////////////////////////////////////////////////

	if (contrast_SHA256(read_sha_256, sha_256) == true)
	{
		printf("check OK\r\n");
		return true;
	}
	else
	{
		printf("check failed\r\n");
		return false;
	}
}

const esp_partition_t *Psysmsg_partition = NULL;
const esp_partition_t *Psetmsg_partition = NULL;

void msg_init(void)
{
	esp_partition_t sysmsg_partition;
	esp_partition_t setmsg_partition;

	LocalState_data = xQueueCreate(1, sizeof(PANEL_LocalState));

	printf("FIND SYSMSG partition\r\n");
	Psysmsg_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
												 ESP_PARTITION_SUBTYPE_DATA_FAT, SYS_PARTITION_NAME);
	if (Psysmsg_partition != NULL)
	{
		printf("partiton addr: 0x%08x; size: %d; label: %s\n", Psysmsg_partition->address, Psysmsg_partition->size, Psysmsg_partition->label);
	}
	else
	{
		printf("Partition error: can't find partition name: %s\n", SYS_PARTITION_NAME);
	}

	sysmsg_partition.address = Psysmsg_partition->address;
	sysmsg_partition.size = Psysmsg_partition->size;
	sysmsg_partition.type = Psysmsg_partition->type;
	memcpy(sysmsg_partition.label, Psysmsg_partition->label, sizeof(int));
	sysmsg_partition.subtype = Psysmsg_partition->subtype;
	sysmsg_partition.encrypted = Psysmsg_partition->encrypted;

	if (check_sha256(&sysmsg_partition) == true)
	{
		printf("SYSMSG flash check OK\n");
	}
	else
	{
		printf("SYSMSG partition error\r\n");
		printf("Erasing SYSMSG flash\r\n");
		printf("Erase size: %d Bytes\r\n", sysmsg_partition.size);
		ESP_ERROR_CHECK(esp_partition_erase_range(Psysmsg_partition, 0, Psysmsg_partition->size));
		updata_sha256(&sysmsg_partition);
	}

	printf("\r\nFIND SETMSG partition\r\n");
	Psetmsg_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
												 ESP_PARTITION_SUBTYPE_DATA_FAT, SET_PARTITION_NAME);
	if (Psetmsg_partition != NULL)
	{
		printf("partiton addr: 0x%08x; size: %d; label: %s\n", Psetmsg_partition->address, Psetmsg_partition->size, Psetmsg_partition->label);
	}
	else
	{
		printf("Partition error: can't find partition name: %s\n", SET_PARTITION_NAME);
	}

	setmsg_partition.address = Psetmsg_partition->address;
	setmsg_partition.size = Psetmsg_partition->size;
	setmsg_partition.type = Psetmsg_partition->type;
	memcpy(setmsg_partition.label, Psetmsg_partition->label, sizeof(Psetmsg_partition->label));
	setmsg_partition.subtype = Psetmsg_partition->subtype;
	setmsg_partition.encrypted = Psetmsg_partition->encrypted;

	if (check_sha256(&setmsg_partition) == true)
	{
		printf("%s partition check OK\n", Psetmsg_partition->label);
	}
	else
	{
		printf("%s partition error\r\n", Psetmsg_partition->label);
		printf("Erasing %s partition\r\n", Psetmsg_partition->label);
		printf("Erase size: %d Bytes\r\n", setmsg_partition.size);
		ESP_ERROR_CHECK(esp_partition_erase_range(&setmsg_partition, 0, setmsg_partition.size));
		updata_sha256(&setmsg_partition);
	}
}

void sysmsg_handle_task(void *arg)
{
	PANEL_SYSInfo *p_sys = &sysmsg;
	sys_data_msg_T sys_handle_msg;

	char *p_sysmsg = (char *)p_sys;

	memset(p_sys, 0, sizeof(PANEL_SYSInfo));
	esp_partition_read(Psysmsg_partition, 0, p_sys, sizeof(sysmsg));

	//	for(int i = 0; i < sizeof(sysmsg); i++){
	//		if(p_sysmsg[i] == 0xff){
	//			p_sysmsg[i] = 0x00;
	//		}
	//	}
	const esp_partition_t *running = esp_ota_get_running_partition();
	esp_app_desc_t running_app_info;
	if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
	{
		ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
	}
	bool updata_sys_msg = false;
	for (int i = 0; i < 32; i++)
	{
		if (running_app_info.version[i] != sysmsg.Software_ver[i])
		{
			updata_sys_msg = true;
			break;
		}
	}

	if (updata_sys_msg)
	{
		//	    uint8_t mac_address[6] = {0};
		printf("Updata SYSMSG\r\n");
		memset(p_sys, 0, sizeof(PANEL_SYSInfo));
		esp_efuse_mac_get_default(sysmsg.MAC_addres);
		sysmsg.Hardware_ver = Hardware_version;
		memset(sysmsg.Software_ver, 0, sizeof(sysmsg.Software_ver));
		memcpy(sysmsg.Software_ver, running_app_info.version, sizeof(sysmsg.Software_ver));
		sysmsg.Main_type = 0x0f;
		sysmsg.PANEL_model = Recognition_GETWAY;
		memset(sysmsg.WN_char, 0, sizeof(sysmsg.WN_char));
		memcpy(sysmsg.WN_char, "nihaoxiaozhi", sizeof(sysmsg.WN_char));
		sysmsg.fuction = Offline_Recognition;
		sysmsg.boot_voice_lev = vol;
		sysmsg.free_RAM = 0;
		sysmsg.MN_Num = 96;
		sysmsg.free_MN_Num = 0;
		sysmsg.Button_Num = 1;
		if (memcmp(running->label, "ota_0", strlen("ota_0")) == 0)
		{
			sysmsg.BOOT_Partition = 0;
		}
		else if (memcmp(running->label, "ota_1", strlen("ota_1")) == 0)
		{
			sysmsg.BOOT_Partition = 1;
		}
		else
		{
			sysmsg.BOOT_Partition = 0xff;
		}
		sysmsg.Selftest = 1;

		ESP_ERROR_CHECK(esp_partition_erase_range(Psysmsg_partition, 0, Psysmsg_partition->size));
		esp_partition_write(Psysmsg_partition, 0, p_sysmsg, Psysmsg_partition->size);
		updata_sha256(Psysmsg_partition);
		printf("Updata SYSMSG success\r\n");
		memset(p_sys, 0, sizeof(PANEL_SYSInfo));
		esp_partition_read(Psysmsg_partition, 0, p_sys, sizeof(sysmsg));
	}
	else
	{
		printf("Don't need updata SYSMSG\r\n");
	}

	printf("read MAC_addres: %02x.%02x.%02x.%02x.%02x.%02x\r\n", sysmsg.MAC_addres[0], sysmsg.MAC_addres[1], sysmsg.MAC_addres[2], sysmsg.MAC_addres[3], sysmsg.MAC_addres[4], sysmsg.MAC_addres[5]);
	printf("read Hardware_ver: %d\r\n", sysmsg.Hardware_ver);
	printf("read Software_ver: %.*s\r\n", sizeof(sysmsg.Software_ver), sysmsg.Software_ver);
	printf("read Main_type: %d\r\n", sysmsg.Main_type);
	printf("read PANEL_model: %d\r\n", sysmsg.PANEL_model);
	printf("read fuction: %d\r\n", sysmsg.fuction);
	printf("read boot_voice_lev: %d\r\n", sysmsg.boot_voice_lev);
	printf("read free_RAM: %d\r\n", sysmsg.free_RAM);
	printf("read MN_Num: %d\r\n", sysmsg.MN_Num);
	printf("read free_MN_Num: %d\r\n", sysmsg.free_MN_Num);
	printf("read Button_Num: %d\r\n", sysmsg.Button_Num);
	printf("read BOOT_Partition: %d\r\n", sysmsg.BOOT_Partition);
	printf("read Selftest: %d\r\n", sysmsg.Selftest);

	while (1)
	{
		xQueueReceive(sys_handle_data, (void *)&sys_handle_msg, portMAX_DELAY);
		if (sys_handle_msg.readwrite == true)
		{ //读取系统信息

			ESP_LOGI(TAG, "Read sys msg");

			//			memset(p_sysmsg, 0, sizeof(sysmsg));
			esp_partition_read(Psysmsg_partition, 0, p_sysmsg, sizeof(sysmsg));

			Send_msg_T SendMSG;

			SendMSG.send_task = 1;
			SendMSG.sendlen = sizeof(sysmsg);

			char *sysmsg_buf = malloc(sizeof(sysmsg));
			memcpy(sysmsg_buf, p_sysmsg, sizeof(sysmsg));
			SendMSG.buf = sysmsg_buf;

			xQueueSend(sdk_send_data, (void *)&SendMSG, 10);

			printf("p_sysmsg: %p\n", sysmsg_buf);

			printf("read MAC_addres: %02x.%02x.%02x.%02x.%02x.%02x\r\n", sysmsg.MAC_addres[0], sysmsg.MAC_addres[1], sysmsg.MAC_addres[2], sysmsg.MAC_addres[3], sysmsg.MAC_addres[4], sysmsg.MAC_addres[5]);
			printf("read Hardware_ver: %d\r\n", sysmsg.Hardware_ver);
			printf("read Software_ver: %.*s\r\n", sizeof(sysmsg.Software_ver), sysmsg.Software_ver);
			printf("read Main_type: %d\r\n", sysmsg.Main_type);
			printf("read PANEL_model: %d\r\n", sysmsg.PANEL_model);
			printf("read fuction: %d\r\n", sysmsg.fuction);
			printf("read boot_voice_lev: %d\r\n", sysmsg.boot_voice_lev);
			printf("read free_RAM: %d\r\n", sysmsg.free_RAM);
			printf("read MN_Num: %d\r\n", sysmsg.MN_Num);
			printf("read free_MN_Num: %d\r\n", sysmsg.free_MN_Num);
			printf("read Button_Num: %d\r\n", sysmsg.Button_Num);
			printf("read BOOT_Partition: %d\r\n", sysmsg.BOOT_Partition);
			printf("read Selftest: %d\r\n", sysmsg.Selftest);
		}
		else
		{ //更新系统信息,保留

		}
	}
}

void setmsg_handle_task(void *arg)
{
	PANEL_SetInfo *p_set = &setmsg;
	set_data_msg_T set_handle_msg;
	char *p_setmsg = (char *)p_set;

	esp_partition_read(Psetmsg_partition, 0, p_setmsg, sizeof(setmsg));

	//	for(int i = 0; i < sizeof(setmsg); i++){
	//		if(p_setmsg[i] == 0xff){
	//			p_setmsg[i] = 0x00;
	//		}
	//	}

	//	printf("2: %d %p %p\r\n", sizeof(setmsg), p_set, p_setmsg);
	//	for(int i = 0; i < sizeof(setmsg); i++){
	//		printf("%02x", (unsigned int)p_setmsg[i]);
	//	}
	//	printf("\r\n2 end\r\n");

	printf("read PANEL_did: %08x\r\n", setmsg.PANEL_did);
	printf("read PANEL_name: %.*s\r\n", sizeof(setmsg.PANEL_name), setmsg.PANEL_name);
	printf("updata PANEL_room: %.*s\r\n", sizeof(setmsg.PANEL_room), setmsg.PANEL_room);
	printf("read Mute: %d\r\n", setmsg.Mute);
	printf("read OnlineIP: %d.%d.%d.%d\r\n", setmsg.OnlineInfo.OnlineIP[0], setmsg.OnlineInfo.OnlineIP[1], setmsg.OnlineInfo.OnlineIP[2], setmsg.OnlineInfo.OnlineIP[3]);
	printf("read OnlineName: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlineName), setmsg.OnlineInfo.OnlineName);
	printf("read OnlinePWD: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlinePWD), setmsg.OnlineInfo.OnlinePWD);
	printf("read OnlinePort: %d\r\n", setmsg.OnlineInfo.OnlinePort);
	printf("read Voice_Lev: %d\r\n", setmsg.Voice_Lev);
	printf("read Room_Lev: %d\r\n", setmsg.Room_Lev);

	while (1)
	{
		xQueueReceive(set_handle_data, (void *)&set_handle_msg, portMAX_DELAY);

		printf("SETMSG handle \r\n");
		if (set_handle_msg.readwrite == true)
		{
			ESP_LOGI(TAG, "Read set msg");
			//			memset(p_setmsg, 0, sizeof(setmsg));
			esp_partition_read(Psetmsg_partition, 0, p_setmsg, sizeof(setmsg));

			Send_msg_T SendMSG;

			SendMSG.send_task = 2;
			SendMSG.sendlen = sizeof(setmsg);

			char *setmsg_buf = malloc(sizeof(setmsg));
			memcpy(setmsg_buf, p_setmsg, sizeof(setmsg));
			SendMSG.buf = setmsg_buf;
			xQueueSend(sdk_send_data, (void *)&SendMSG, 10);

			printf("p_sysmsg: %p\n", setmsg_buf);

			printf("read PANEL_did: %08x\r\n", setmsg.PANEL_did);
			printf("read PANEL_name: %.*s\r\n", sizeof(setmsg.PANEL_name), setmsg.PANEL_name);
			printf("read PANEL_room: %04x\r\n", ((setmsg.PANEL_room[0] << 8) + setmsg.PANEL_room[1]));
			printf("read Mute: %d\r\n", setmsg.Mute);
			printf("read OnlineIP: %d.%d.%d.%d\r\n", setmsg.OnlineInfo.OnlineIP[0], setmsg.OnlineInfo.OnlineIP[1], setmsg.OnlineInfo.OnlineIP[2], setmsg.OnlineInfo.OnlineIP[3]);
			printf("read OnlineName: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlineName), setmsg.OnlineInfo.OnlineName);
			printf("read OnlinePWD: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlinePWD), setmsg.OnlineInfo.OnlinePWD);
			printf("read OnlinePort: %d\r\n", setmsg.OnlineInfo.OnlinePort);
			printf("read Voice_Lev: %d\r\n", setmsg.Voice_Lev);
			printf("read Room_Lev: %d\r\n", setmsg.Room_Lev);
		}
		else
		{
			ESP_LOGI(TAG, "updata set msg");

			bzero(p_setmsg, sizeof(setmsg));

			char *char_pp = (char *)set_handle_msg.buf;
			ESP_LOGI(TAG, "2.3: %d %p %p %p\r\n", sizeof(setmsg), p_set, p_setmsg, set_handle_msg.buf);
			for (int i = 0; i < sizeof(setmsg); i++)
			{
				printf("%02x", (unsigned int)char_pp[i]);
			}
			ESP_LOGI(TAG, "\r\n2.3 end\r\n");

			//			printf("2.5: %d %p %p %p\r\n", sizeof(setmsg), p_set, p_setmsg, set_handle_msg.buf);
			//			for(int i = 0; i < sizeof(setmsg); i++){
			//				printf("%02x", (unsigned int)p_setmsg[i]);
			//			}
			//			printf("\r\n2.5 end\r\n");

			memcpy(p_setmsg, set_handle_msg.buf, sizeof(setmsg));

			printf("updata PANEL_did: %08x\r\n", setmsg.PANEL_did);
			printf("updata PANEL_name: %.*s\r\n", sizeof(setmsg.PANEL_name), setmsg.PANEL_name);
			printf("updata PANEL_room: %04x\r\n", ((setmsg.PANEL_room[0] << 8) + setmsg.PANEL_room[1]));
			printf("updata Mute: %d\r\n", setmsg.Mute);
			printf("updata OnlineIP: %d.%d.%d.%d\r\n", setmsg.OnlineInfo.OnlineIP[0], setmsg.OnlineInfo.OnlineIP[1], setmsg.OnlineInfo.OnlineIP[2], setmsg.OnlineInfo.OnlineIP[3]);
			printf("updata OnlineName: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlineName), setmsg.OnlineInfo.OnlineName);
			printf("updata OnlinePWD: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlinePWD), setmsg.OnlineInfo.OnlinePWD);
			printf("updata OnlinePort: %d\r\n", setmsg.OnlineInfo.OnlinePort);
			printf("updata Voice_Lev: %d\r\n", setmsg.Voice_Lev);
			printf("updata Room_Lev: %d\r\n", setmsg.Room_Lev);

			printf("3: %d %p %p\r\n", sizeof(setmsg), p_set, p_setmsg);
			for (int i = 0; i < sizeof(setmsg); i++)
			{
				printf("%02x", (unsigned int)p_setmsg[i]);
			}
			printf("\r\n3 end\r\n");

			ESP_ERROR_CHECK(esp_partition_erase_range(Psetmsg_partition, 0, Psetmsg_partition->size));

			esp_partition_write(Psetmsg_partition, 0, p_setmsg, Psetmsg_partition->size);
			updata_sha256(Psetmsg_partition);
			if (check_sha256(Psetmsg_partition) == true)
			{
				printf("SETMSG Updata OK\n");
				memset(p_setmsg, 0, sizeof(setmsg));
				esp_partition_read(Psetmsg_partition, 0, p_setmsg, sizeof(setmsg));

				//		    	for(int i = 0; i < sizeof(sysmsg); i++){
				//		    		if(p_setmsg[i] == 0xff){
				//		    			p_setmsg[i] = 0x00;
				//		    		}
				//		    	}

				printf("4: %d %p %p\r\n", sizeof(setmsg), p_set, p_setmsg);
				for (int i = 0; i < sizeof(setmsg); i++)
				{
					printf("%02x", (unsigned int)p_setmsg[i]);
				}
				printf("\r\n4 end\r\n");

				printf("updata PANEL_did: %08x\r\n", setmsg.PANEL_did);
				printf("updata PANEL_name: %.*s\r\n", sizeof(setmsg.PANEL_name), setmsg.PANEL_name);
				printf("updata PANEL_room: %04x\r\n", ((setmsg.PANEL_room[0] << 8) + setmsg.PANEL_room[1]));
				printf("updata Mute: %d\r\n", setmsg.Mute);
				printf("updata OnlineIP: %d.%d.%d.%d\r\n", setmsg.OnlineInfo.OnlineIP[0], setmsg.OnlineInfo.OnlineIP[1], setmsg.OnlineInfo.OnlineIP[2], setmsg.OnlineInfo.OnlineIP[3]);
				printf("updata OnlineName: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlineName), setmsg.OnlineInfo.OnlineName);
				printf("updata OnlinePWD: %.*s\r\n", sizeof(setmsg.OnlineInfo.OnlinePWD), setmsg.OnlineInfo.OnlinePWD);
				printf("updata OnlinePort: %d\r\n", setmsg.OnlineInfo.OnlinePort);
				printf("updata Voice_Lev: %d\r\n", setmsg.Voice_Lev);
				printf("updata Room_Lev: %d\r\n", setmsg.Room_Lev);
			}
			else
			{
				printf("SETMSG Updata failed\r\n");
				printf("Erasing SETMSG partition\r\n");
				printf("Erase size: %d Bytes\r\n", Psetmsg_partition->size);
				ESP_ERROR_CHECK(esp_partition_erase_range(Psetmsg_partition, 0, Psetmsg_partition->size));
				updata_sha256(Psetmsg_partition);
			}
		}
	}
}

void functionstate(void)
{
	PANEL_FunctionState FunctionState;
	pPANEL_FunctionState pFunctionState = &FunctionState;

	int localstate = 0;
	xQueueReceive(LocalState_data, (void *)&localstate, 30);

	FunctionState.LocalState = localstate;
	FunctionState.NetState = Offline;

	char *FunctionState_buf = malloc(sizeof(PANEL_FunctionState));
	Send_msg_T SendMSG;

	memcpy(FunctionState_buf, pFunctionState, sizeof(PANEL_FunctionState));
	SendMSG.send_task = 8;
	SendMSG.buf = FunctionState_buf;
	SendMSG.sendlen = sizeof(PANEL_FunctionState);
	xQueueSend(sdk_send_data, (void *)&SendMSG, 10);
}
