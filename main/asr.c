#include "asr.h"
#include "includes.h"

#include "esp_ns.h"   //单声道人声增强
#include "esp_agc.h"  //增益自动控制
#include "esp_mase.h" //多声道降噪增强
#include "esp_aec.h"  //过滤扬声器输出的声音
#include "ringbuf.h"

#include "play_voice.h"
#include "user_i2c.h"
#include "driver/uart.h"
#include "out_record.h"

#include "sysmsg.h"
#include "PANEL_sdk.h"

voice_cache_msg_T voice_cache;

struct ringbuf *RAW_rb = NULL;
struct ringbuf *AGC_rb = NULL;
struct ringbuf *NS_rb = NULL;
struct ringbuf *mase_rb = NULL;

#define USE_MASE 0
#if USE_MASE
#else
#define USE_NS 1
#endif

#define USE_AGC 1

#if USE_AGC
#define MN_READ AGC_rb
#if USE_MASE
#define AGC_READ mase_rb
#elif USE_NS
#define AGC_READ NS_rb
#endif
#elif USE_MASE
#define MN_READ mase_rb
#elif USE_NS
#define MN_READ NS_rb
#else
#define MN_READ RAW_rb
#endif

#define MASE_FRAME_BYTES 512 //16000*0.016*2=512
#define NS_FRAME_BYTES 960   //16000*0.03*2=960
#define AGC_FRAME_BYTES 320  //16000*0.01*2=320
#define mn_threshold 0.6

#if USE_MASE
void MASE_task(void *arg);
#elif USE_NS
void NS_task(void *arg);
#endif

#if USE_AGC
void AGC_task(void *arg);
#endif

void speech_task(void *arg)
{

#if RECORD_IN_FLASH_EN
    const esp_partition_t *data_partition = NULL;
    int flash_wr_size = 0;
    data_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
                                              ESP_PARTITION_SUBTYPE_DATA_FAT, RECORD_PARTITION_NAME);
#endif

#if OUT_VOICE_SPI
    int buf_offest = 0;
    char *out_voice_buf = (char *)malloc(out_voice_maxsize);
#endif

    esp_wn_iface_t *wakenet;
    model_coeff_getter_t *model_coeff_getter;
    model_iface_data_t *model_wn_data;
    const esp_mn_iface_t *multinet = &MULTINET_MODEL;

    //	uint8_t* flash_read_buff = (uint8_t*) calloc((int)0.03*16000, sizeof(short));
    ESP_LOGI(TAG, "Initialize SR wn handle");

    get_wakenet_iface(&wakenet);
    get_wakenet_coeff(&model_coeff_getter);
    model_wn_data = wakenet->create(model_coeff_getter, DET_MODE_90);
    int wn_num = wakenet->get_word_num(model_wn_data);
    for (int i = 1; i <= wn_num; i++)
    {
        char *name = wakenet->get_word_name(model_wn_data, i);
        ESP_LOGI(TAG, "keywords: %s (index = %d)", name, i);
    }
    float wn_threshold = wakenet->get_det_threshold(model_wn_data, 1);
    int wn_sample_rate = wakenet->get_samp_rate(model_wn_data);
    int audio_wn_chunksize = wakenet->get_samp_chunksize(model_wn_data);
    ESP_LOGI(TAG, "wn: keywords_num = %d, threshold = %f, sample_rate = %d, chunksize = %d, sizeof_uint16 = %d", wn_num, wn_threshold, wn_sample_rate, audio_wn_chunksize, sizeof(int16_t));

    model_iface_data_t *model_mn_data = multinet->create(&MULTINET_COEFF, 6000);
    int audio_mn_chunksize = multinet->get_samp_chunksize(model_mn_data);
    int mn_num = multinet->get_samp_chunknum(model_mn_data);
    int mn_sample_rate = multinet->get_samp_rate(model_mn_data);
    multinet->set_det_threshold(model_mn_data, mn_threshold);
    ESP_LOGI(TAG, "mn: keywords_num = %d , sample_rate = %d, chunksize = %d, mn_threshold %f, sizeof_uint16 = %d", mn_num, mn_sample_rate, audio_mn_chunksize, mn_threshold, sizeof(int16_t));

    int size = audio_wn_chunksize;
    if (audio_mn_chunksize > audio_wn_chunksize)
    {
        size = audio_mn_chunksize;
    }

    int16_t *raw_buffer = (int16_t *)malloc(size * sizeof(short));

    //    int16_t *ns_buffer = (int16_t *)malloc(size * sizeof(short));
    int16_t *out_buffer = (int16_t *)malloc(size * sizeof(short));

    audio_pipeline_handle_t pipeline;
    audio_element_handle_t i2s_stream_reader, filter, raw_read;
    uint32_t mn_count = 0;
    bool enable_wn = true;

    ESP_LOGI(TAG, "[ 1 ] Start codec chip");
    //    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    //    audio_board_sdcard_init(esp_periph_set_init(&periph_cfg));

    ESP_LOGI(TAG, "[ 2.0 ] Create audio pipeline for recording");
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);
    mem_assert(pipeline);

    ESP_LOGI(TAG, "[ 2.1 ] Create i2s stream to read audio data from codec chip");
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.i2s_config.sample_rate = 48000;
    i2s_cfg.type = AUDIO_STREAM_READER;

    i2s_stream_reader = i2s_stream_init(&i2s_cfg);

    ESP_LOGI(TAG, "[ 2.2 ] Create filter to resample audio data");
    rsp_filter_cfg_t rsp_cfg = DEFAULT_RESAMPLE_FILTER_CONFIG();
    rsp_cfg.src_rate = 48000;
    rsp_cfg.src_ch = 2;
#if USE_MASE
    rsp_cfg.dest_rate = 16000;
    rsp_cfg.dest_ch = 2;
#else
    rsp_cfg.dest_rate = 16000;
    rsp_cfg.dest_ch = 1;
#endif
    filter = rsp_filter_init(&rsp_cfg);

    ESP_LOGI(TAG, "[ 2.3 ] Create raw to receive data");
    raw_stream_cfg_t raw_cfg = {
        .out_rb_size = 8 * 1024,
        .type = AUDIO_STREAM_READER,
    };
    raw_read = raw_stream_init(&raw_cfg);

    ESP_LOGI(TAG, "[ 3 ] Register all elements to audio pipeline");
    audio_pipeline_register(pipeline, i2s_stream_reader, "i2s");
    audio_pipeline_register(pipeline, raw_read, "raw");
    audio_pipeline_register(pipeline, filter, "filter");

    ESP_LOGI(TAG, "[ 4 ] Link elements together [codec_chip]-->i2s_stream-->filter-->raw-->[SR]");
    const char *link_tag[3] = {"i2s", "filter", "raw"};
    audio_pipeline_link(pipeline, &link_tag[0], 3);

    ESP_LOGI(TAG, "[ 5 ] Initialize Noise suppression and AGC");

    //    void *agc_handle = esp_agc_open(3, 16000);
    //    set_agc_config(agc_handle, 15, 1, 3);

    RAW_rb = rb_create(8 * 1024, 1);
#if USE_MASE
    mase_rb = rb_create(8 * 1024, 1);
    xTaskCreatePinnedToCore(MASE_task, "mase", 2 * 1024, NULL, 11, NULL, 1);
#endif
#if USE_NS
    NS_rb = rb_create(8 * 1024, 1);
    xTaskCreatePinnedToCore(NS_task, "ns", 2 * 1024, NULL, 11, NULL, 1);
#endif

#if USE_AGC
    AGC_rb = rb_create(8 * 1024, 1);
    xTaskCreatePinnedToCore(AGC_task, "agc", 2 * 1024, NULL, 11, NULL, 1);
#endif

    xSemaphoreGive(asrinit_finish);

    //   xSemaphoreTake(play_finish, portMAX_DELAY);
    ESP_LOGI(TAG, "[ 6 ] Start audio_pipeline");
    audio_pipeline_run(pipeline);

    SecInterctive = xSemaphoreCreateBinary();

    int play_list = -1;
    int state = 0;
    int localstate = 0;
    while (1)
    {
        raw_stream_read(raw_read, (char *)raw_buffer, size * sizeof(short));
        rb_write(RAW_rb, (char *)raw_buffer, size * sizeof(short), portMAX_DELAY);
        //        ns_process(ns_inst, raw_buffer, out_buffer);
        //        for(int i = 0; i < 3; i++)
        //        {
        //        	esp_agc_process(agc_handle, ns_buffer + (i * 160), out_buffer + (i * 160), 160, 16000);
        //        	printf("25646\r\n");
        //        }

        //    	esp_agc_process(agc_handle, ns_buffer, out_buffer, 30, 16000);
        //        if(rb_read(AGC_rb, (char *)out_buffer, size * sizeof(short), 15))
        //        {
        //        	printf("enable_wn");
        if (xSemaphoreTake(SecInterctive, 0))
        {
            enable_wn = false;
        }
        if (enable_wn)
        {
            rb_read(MN_READ, (char *)out_buffer, size * sizeof(short), 0);

            localstate = Normal_Mode;
            xQueueOverwrite(LocalState_data, (void *)&localstate); //覆盖写入,输出最新状态
            if (wakenet->detect(model_wn_data, (int16_t *)raw_buffer) == WAKE_UP)
            {
                play_list = nihao;
                xQueueSend(play_voice_task, (void *)&play_list, 0);
                ESP_LOGI(TAG, "\n\nwake up	wake up	wake up	wake up	wake up\n\n");
                enable_wn = false;
                xQueueReset(asr_state);
                state = wakeup;
                xQueueOverwrite(asr_state, (void *)&state);
                xSemaphoreTake(play_finish, portMAX_DELAY);
                rb_reset(MN_READ);

#if OUT_VOICE_SPI
                memset(out_voice_buf, 0, out_voice_maxsize);
                buf_offest = 0;
#endif
            }
            else
            {
                state = sleep;
                xQueueOverwrite(asr_state, (void *)&state);
            }
        }
        else
        {
            mn_count++;
            gpio_set_level(22, 1);
            rb_read(MN_READ, (char *)out_buffer, size * sizeof(short), portMAX_DELAY);
            localstate = WakeUp_Mode;
            xQueueOverwrite(LocalState_data, (void *)&localstate); //覆盖写入,输出最新状态
#if RECORD_IN_FLASH_EN
            esp_partition_write(data_partition, flash_wr_size, raw_buffer, size * sizeof(short));
            flash_wr_size += size * sizeof(short);
#endif

#if OUT_VOICE_SPI
            memcpy(out_voice_buf + buf_offest, out_buffer, size * sizeof(short));
            buf_offest += size * sizeof(short);
            voice_cache.buf = out_voice_buf;
            voice_cache.cache_size = buf_offest;
            xQueueOverwrite(voice_cache_size, (void *)&voice_cache); //覆盖写入，直接按最新的数量输出
#endif

            float commit_id = multinet->detect(model_mn_data, out_buffer);
            //            ESP_LOGI(TAG, "\ncommit_id:%f  commit_id:%f  commit_id:%f  commit_id:%f  \n\n",commit_id,commit_id,commit_id,commit_id);
            //                if (asr_multinet_control((int)commit_id) == ESP_OK  ) {
            if (commit_id >= 0)
            {

                PANEL_Recognition Recognition_msg;
                pPANEL_Recognition pRecognition_msg = &Recognition_msg;
                char *char_pRecognition_msg = (char *)pRecognition_msg;
                memcpy(Recognition_msg.PANEL_room, setmsg.PANEL_room, sizeof(setmsg.PANEL_room));
                Recognition_msg.PANEL_did = setmsg.PANEL_did;
                Recognition_msg.Recognition_Num = commit_id;
                Recognition_msg.Matching_Rate = 0xff;
                Recognition_msg.Use_Rate = 0xff;
                Recognition_msg.Speech_Lev = 0xff;
                Recognition_msg.Noice_Lev = 0xff;
                Recognition_msg.Room_Lev = setmsg.Room_Lev;
                Send_msg_T SendMSG;

                SendMSG.send_task = 3;
                SendMSG.sendlen = sizeof(Recognition_msg);
                SendMSG.buf = char_pRecognition_msg;

                char *Recognition_msg_buf = malloc(sizeof(PANEL_Recognition));
                memcpy(Recognition_msg_buf, char_pRecognition_msg, sizeof(PANEL_Recognition));
                SendMSG.buf = Recognition_msg_buf;
                xQueueSend(sdk_send_data, (void *)&SendMSG, 10);

                printf("pRecognition_msg: %p\r\n", Recognition_msg_buf);
                //                	char *dtmp = (char *)asr_2_uart + 50 * (int)commit_id;
                //                	uart_write_bytes(EX_UART_NUM, (const char*)dtmp, strlen(asr_2_uart[(int)commit_id]) + 1);
                //                	char uart1out[30] = {0};
                //                	sprintf(uart1out, "speech recognition: %02d\r\n", (int)commit_id);
                //                	uart_write_bytes(EX_UART_NUM, (const char*)uart1out, strlen(uart1out) + 1);
                state = multinet_faild;
                xQueueOverwrite(asr_state, (void *)&state);
                ESP_LOGI(TAG, "\ncommit_id:%f  commit_id:%f  commit_id:%f  commit_id:%f  \n\n", commit_id, commit_id, commit_id, commit_id);
                rb_reset(AGC_rb);
                play_list = haode;
                // xQueueSend(play_voice_task, (void *)&play_list, 0);
                // i2c_master_read_slave(9);
                enable_wn = true;
                mn_count = 0;
                gpio_set_level(22, 0);
#if RECORD_IN_FLASH_EN
                xQueueSend(record_size, (void *)&flash_wr_size, 0);
                vTaskDelay(pdMS_TO_TICKS(5));
                flash_wr_size = 0;
#endif
            }
            else if (mn_count == mn_num)
            {
                ESP_LOGI(TAG, "\nstop multinet  stop multinet  stop multinet  stop multinet  \n");
                state = multinet_faild;
                xQueueOverwrite(asr_state, (void *)&state);
                play_list = wmtqc;
                rb_reset(AGC_rb);
                xQueueSend(play_voice_task, (void *)&play_list, 0);
                i2c_master_read_slave(9);
                enable_wn = true;
                mn_count = 0;
                gpio_set_level(22, 0);

#if RECORD_IN_FLASH_EN
                xQueueSend(record_size, (void *)&flash_wr_size, 0);
                vTaskDelay(pdMS_TO_TICKS(5));
                flash_wr_size = 0;
#endif
            }
            else
            {
                state = asr_multinet;
            }
        }
        //        }
    }
}

#if USE_MASE
void MASE_task(void *arg)
{
    int nch = 2;
    int16_t *mase_read = malloc(MASE_FRAME_BYTES * nch);
    int16_t *mase_in = malloc(MASE_FRAME_BYTES * nch);
    void *mase_handle = mase_create(16000, MASE_FRAME_SIZE, TWO_MIC_LINE, 80, WAKE_UP_ENHANCEMENT_MODE, 0);

    int16_t *mase_out = malloc(MASE_FRAME_BYTES);
    while (1)
    {
        rb_read(RAW_rb, (char *)mase_read, MASE_FRAME_BYTES * nch, portMAX_DELAY);
        for (int i = 0; i < MASE_FRAME_BYTES / 2; i += 2)
        {
            mase_in[i] = mase_read[i];
            mase_in[i + MASE_FRAME_BYTES / 2] = mase_read[i + 1];
        }
        mase_process(mase_handle, mase_in, mase_out);
        rb_write(mase_rb, (char *)mase_out, MASE_FRAME_BYTES, portMAX_DELAY);
    }
}
#endif

#if USE_NS
void NS_task(void *arg)
{
    int16_t *ns_in = malloc(NS_FRAME_BYTES);
    int16_t *ns_out = malloc(NS_FRAME_BYTES);
    ns_handle_t ns_inst = ns_create(30);
    while (1)
    {
        rb_read(RAW_rb, (char *)ns_in, NS_FRAME_BYTES, portMAX_DELAY);
        //        printf("ns");
        ns_process(ns_inst, ns_in, ns_out);
        rb_write(NS_rb, (char *)ns_out, NS_FRAME_BYTES, portMAX_DELAY);
    }
}
#endif

#if USE_AGC
void AGC_task(void *arg)
{
    int16_t *agc_in = malloc(AGC_FRAME_BYTES);
    int16_t *agc_out = malloc(AGC_FRAME_BYTES);

    void *agc_handle = esp_agc_open(3, 16000);
    set_agc_config(agc_handle, 15, 1, 3);

    int _err_step = 1;
    if (0 == (agc_in && _err_step++ && agc_out && _err_step++ && agc_handle && _err_step++))
    {
        printf("Failed to apply for memory, err_step = %d", _err_step);
        goto _agc_init_fail;
    }
    while (1)
    {
        rb_read(AGC_READ, (char *)agc_in, AGC_FRAME_BYTES, portMAX_DELAY);
        //        printf("agc");
        esp_agc_process(agc_handle, agc_in, agc_out, AGC_FRAME_BYTES / 2, 16000);
        rb_write(AGC_rb, (char *)agc_out, AGC_FRAME_BYTES, portMAX_DELAY);
    }
_agc_init_fail:
    if (agc_in)
    {
        free(agc_in);
        agc_in = NULL;
    }
    if (agc_out)
    {
        free(agc_out);
        agc_out = NULL;
    }
    if (agc_handle)
    {
        free(agc_handle);
        agc_handle = NULL;
    }
    vTaskDelete(NULL);
}
#endif
