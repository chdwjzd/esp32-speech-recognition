#include "includes.h"

#include "play_voice.h"
#include "asr.h"
#include "sync_stats.h"
#include "user_i2c.h"
#include "out_record.h"
#include "user_uart.h"
#include "ota.h"
#include "user_spi.h"

#include "sysmsg.h"
#include "PANEL_sdk.h"

char *TAG = "P1 TEST";

void app_main(void)
{

    gpio_config_t gpio_conf = {
        .pin_bit_mask = 1UL << get_green_led_gpio(),
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = 0,
        .pull_down_en = 0,
        .intr_type = 0};
    gpio_config(&gpio_conf);

    gpio_set_level(22, 1);
    gpio_set_level(21, 0);

    esp_log_level_set("*", ESP_LOG_WARN);
    esp_log_level_set(TAG, ESP_LOG_INFO);

    printf("%s\r\n", TAG);
    ESP_LOGI(TAG, "OTA TEST 4");
    ESP_LOGI(TAG, "ADD MN TEST 3");
    ESP_LOGI(TAG, "get_green_led_gpio:%d", get_green_led_gpio());

    uint8_t mac_address[6] = {0};
    esp_efuse_mac_get_default(mac_address);
    printf("mac:");
    for (int i = 0; i < 6; i++)
    {
        printf("%02x.", mac_address[i]);
    }
    printf("\r\n");
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
           chip_info.cores,
           (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    gpio_pad_select_gpio(21);
    gpio_set_direction(21, GPIO_MODE_OUTPUT);

    sys_handle_data = xQueueCreate(2, sizeof(sys_data_msg_T));
    set_handle_data = xQueueCreate(2, sizeof(set_data_msg_T));
    sdk_send_data = xQueueCreate(10, sizeof(Send_msg_T));
    sdk_rec_data = xQueueCreate(10, sizeof(rec_msg_T));
    uart_rec = xQueueCreate(3, sizeof(MSG_T));
    asrinit_finish = xSemaphoreCreateBinary();
    asr_state = xQueueCreate(1, sizeof(char));
    play_finish = xSemaphoreCreateBinary();
    boot_success = xSemaphoreCreateBinary();
    start_ota = xQueueCreate(1, sizeof(PANEL_StartOTA));
    ota_ready = xSemaphoreCreateBinary();
    end_ota = xSemaphoreCreateBinary();
    ota_data = xQueueCreate(10, sizeof(ota_cache_msg_T));
    play_voice_task = xQueueCreate(3, sizeof(int));
    spi_rec = xQueueCreate(3, sizeof(SPI_REC_MSG_T));
    spi_2_task = xQueueCreate(3, sizeof(SPI_2_TASK_T));
    spi_send = xQueueCreate(250, sizeof(TASK_2_SPI_MSG_T));

    vTaskDelay(pdMS_TO_TICKS(1000));

    xTaskCreatePinnedToCore(uart_event_task, "uart_task", 4096, NULL, UART_TASK_PRIO, &uartTASK_Handler, 1);

#if show_state
    xTaskCreatePinnedToCore(stats_task, "stats", 5 * 1024, NULL, STATS_TASK_PRIO, &StatsTask_Handler, 1);
#endif

    msg_init();

    xTaskCreatePinnedToCore(sysmsg_handle_task, "sysmsg_handle", 3 * 1024, NULL, sysmsg_handle_PRIO, &sysmsg_task_Handler, 1);
    vTaskDelay(pdMS_TO_TICKS(100));
    xTaskCreatePinnedToCore(setmsg_handle_task, "setmsg_handle", 3 * 1024, NULL, setmsg_handle_PRIO, &setmsg_task_Handler, 1);

    xTaskCreatePinnedToCore(speech_task, "speech", 20 * 1024, NULL, SPEECH_TASK_PRIO, &asrTASK_Handler, 0);

    xSemaphoreTake(asrinit_finish, portMAX_DELAY);

    //    xTaskCreatePinnedToCore(spi_task, "spi_task", 4 * 1024, NULL, SPI_TASK_PRIO, &SPITASK_Handler, 1);

    xTaskCreatePinnedToCore(play_task, "play_voice", 10 * 1024, NULL, PLAY_TASK_PRIO, &PlayTask_Handler, 1);

    //    xTaskCreatePinnedToCore(out_record, "out_record", 10 * 1024, NULL, RECORD_TASK_PRIO, &outrecordTask_Handler, 1);

    xTaskCreatePinnedToCore(ota_task, "ota_task", 8192, NULL, OTA_TASK_PRIO, &otaTASK_Handler, 1);

    xTaskCreatePinnedToCore(sendmsg_task, "sendmsg_task", 8 * 1024, NULL, 5, &sendmsg_task_Handler, 1);

    xTaskCreatePinnedToCore(recmsg_task, "recmsg_task", 5 * 1024, NULL, 5, &recmsg_task_Handler, 1);

    vTaskDelay(pdMS_TO_TICKS(3000));
    sys_data_msg_T sys_handle_msg;
    sys_handle_msg.readwrite = true;
    xQueueSend(sys_handle_data, (void *)&sys_handle_msg, 10);

    vTaskDelay(pdMS_TO_TICKS(3000));
    set_data_msg_T set_handle_msg;
    set_handle_msg.readwrite = true;
    xQueueSend(set_handle_data, (void *)&set_handle_msg, 10);

    vTaskDelay(pdMS_TO_TICKS(2000));
    i2c_master_read_slave(9);
    vTaskDelay(pdMS_TO_TICKS(100));
    xSemaphoreGive(boot_success);

    ////////////////////////////////		测试用      //////////////////////////////
    PANEL_SetInfo setinfo;
    pPANEL_SetInfo pp = &setinfo;
    char *char_pp = (char *)pp;
    memset(pp, 0, sizeof(PANEL_SetInfo));
    memcpy(setinfo.PANEL_name, "test panel", sizeof(setinfo.PANEL_name));
    setinfo.PANEL_did = 0x01234567;
    char roomid = 0x0003;
    setinfo.PANEL_room[0] = ((roomid >> 8) & 0xff);
    setinfo.PANEL_room[1] = (roomid & 0xff);
    setinfo.Mute = false;
    setinfo.OnlineInfo.OnlinePort = 49989;
    setinfo.OnlineInfo.OnlineIP[0] = 192;

    setinfo.OnlineInfo.OnlineIP[1] = 168;
    setinfo.OnlineInfo.OnlineIP[2] = 1;
    setinfo.OnlineInfo.OnlineIP[3] = 1;
    memcpy(setinfo.OnlineInfo.OnlineName, "chdwjzd", sizeof(setinfo.OnlineInfo.OnlineName));
    memcpy(setinfo.OnlineInfo.OnlinePWD, "992405314@qq.com", sizeof(setinfo.OnlineInfo.OnlinePWD));
    setinfo.Voice_Lev = 0x11;
    setinfo.Room_Lev = 0x00;
    set_data_msg_T test_set_data;
    test_set_data.buf = pp;
    test_set_data.readwrite = false;

    printf("2.2: %d %p %p\r\n", sizeof(setmsg), pp, char_pp);
    for (int i = 0; i < sizeof(setmsg); i++)
    {
        printf("%02x", (unsigned int)char_pp[i]);
    }
    printf("\r\n2.2 end\r\n");

    vTaskDelay(pdMS_TO_TICKS(100));
    xQueueSend(set_handle_data, (void *)&test_set_data, 10);
    vTaskDelay(pdMS_TO_TICKS(5000));
    ////////////////////////////////		测试用 		//////////////////////////////
}
