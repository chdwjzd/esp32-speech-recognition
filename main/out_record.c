#include "out_record.h"
#include "includes.h"

#include "user_spi.h"
#include "asr.h"

char START_END[] = {0x00, 0x00, 0x00, 0x00};
char RIFF[] = {0x52, 0x49, 0x46, 0x46};
char WAVE[] = {0x57, 0x41, 0x56, 0x45};
char FORMAT[] = {0x66, 0x6D, 0x74, 0x20, 0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x80, 0x3E, 0x00, 0x00, 0x00, 0x7D, 0x00, 0x00, 0x02, 0x00, 0x10, 0x00};
char WAVE_DATA[] = {0x64, 0x61, 0x74, 0x61};

TASK_2_SPI_MSG_T spi_send_voice;
voice_cache_msg_T cache_spi;

void big32little(uint32_t in, char *out)
{
	out[0] = in & 0x000000ff;
	out[1] = (in & 0x0000ff00) >> 8;
	out[2] = (in & 0x00ff0000) >> 16;
	out[3] = (in & 0xff000000) >> 24;
}

void out_record(void *arg)
{
#if OUT_RECORD_UART
	const esp_partition_t *data_partition = NULL;

	printf("Erasing RECORD flash \n");
	data_partition = esp_partition_find_first(ESP_PARTITION_TYPE_DATA,
											  ESP_PARTITION_SUBTYPE_DATA_FAT, RECORD_PARTITION_NAME);
	if (data_partition != NULL)
	{
		printf("partiton addr: 0x%08x; size: %d; label: %s\n", data_partition->address, data_partition->size, data_partition->label);
	}
	else
	{
		printf("Partition error: can't find partition name: %s\n", RECORD_PARTITION_NAME);
	}
	printf("Erase size: %d Bytes\n", RECORD_PARTITION_ERASE_SIZE);
	ESP_ERROR_CHECK(esp_partition_erase_range(data_partition, 0, RECORD_PARTITION_ERASE_SIZE));

	record_size = xQueueCreate(2, sizeof(int));

	//    char *flash_read_buff;

	//    flash_read_buff = (char*) malloc(512 * 1024);
	char out_little[4];
	int flash_wr_size = 0;
	printf("RECORD IN FLASH ready\n");
#endif

#if OUT_VOICE_SPI
	int cache_state = 0;
	//    int cache_size = 0;
	int out_size = 0;
	voice_cache_size = xQueueCreate(1, sizeof(voice_cache_msg_T));
#endif
	while (1)
	{

#if OUT_VOICE_SPI

		//    	vTaskDelay(pdMS_TO_TICKS(3000));
		xQueuePeek(asr_state, &(cache_state), pdMS_TO_TICKS(10));

		//    	if(cache_state > 0){
		//    		xQueueReceive(voice_cache_size, &(cache_size), 30);
		//    	}
		memset(&cache_spi, 0, sizeof(cache_spi));
		xQueueReceive(voice_cache_size, (void *)&cache_spi, 30);
		if (cache_spi.cache_size > out_size + 900 || (cache_spi.cache_size > out_size))
		{												  //缓冲大于900字节，或者结束了识别后还有数据没发送
			char *read_cache = (char *)malloc(18 * 1024); //定义一个最大18K的缓存，可以容纳超过0.5的缓存

			memset(read_cache, 0, 18 * 1024);
			memcpy(read_cache, cache_spi.buf + out_size, cache_spi.cache_size - out_size);

			spi_send_voice.buf = read_cache;
			spi_send_voice.send_size = cache_spi.cache_size - out_size;

			spi_send_voice.send_task = 2;
			xQueueSend(spi_send, (void *)&spi_send_voice, 30);
			ESP_LOGI(TAG, "spi_send_voice.send_size= %d; cache_spi.cache_size= %d; out_size= %d; cache_state= %d\r\n", spi_send_voice.send_size, cache_spi.cache_size, out_size, cache_state);
			xQueuePeek(asr_state, &(cache_state), pdMS_TO_TICKS(10));
			if (cache_state == sleep || cache_state == multinet_succes || cache_state == multinet_faild)
			{	//当Sleep或者识别完成的状态时会把剩下的所有数据全部输出
				//        		ESP_LOGI(TAG,"reset cache_state");
				//        		out_size = 0;
				//        		cache_spi.cache_size = 0;
			}
			else
			{
				out_size = cache_spi.cache_size;
			}
		}
		if (cache_state == sleep || cache_state == multinet_succes || cache_state == multinet_faild)
		{	//当Sleep或者识别完成的状态时会把剩下的所有数据全部输出
			//    		ESP_LOGI(TAG,"reset cache_state");
			out_size = 0;
			cache_spi.cache_size = 0;
		}

#endif

#if OUT_RECORD_UART

		xQueueReceive(record_size, &(flash_wr_size), pdMS_TO_TICKS(30));
		if (flash_wr_size > 0)
		{
			vTaskDelay(pdMS_TO_TICKS(2000));
			char *flash_read_uart;
			flash_read_uart = (char *)malloc(512 * 1024);

			memset(flash_read_uart, 0, 512 * 1024);
			esp_partition_read(data_partition, 0, flash_read_uart, flash_wr_size);

			printf("out record start;flash_wr_size: %d\n\n{\n\n", flash_wr_size);

			uart_write_bytes(UART_NUM_1, START_END, sizeof(START_END));

			uart_write_bytes(UART_NUM_1, RIFF, sizeof(RIFF));
			//            for(int i = 0;i < sizeof(RIFF);i++)
			//            {
			//            	printf("%02x ",RIFF[i]);
			//            }

			big32little(flash_wr_size + 36, out_little);

			uart_write_bytes(UART_NUM_1, out_little, sizeof(out_little));
			//            for(int i = 0;i < sizeof(out_little);i++)
			//            {
			//            	printf("%02x ",out_little[i]);
			//            }
			uart_write_bytes(UART_NUM_1, WAVE, sizeof(WAVE));
			//            for(int i = 0;i < sizeof(WAVE);i++)
			//            {
			//            	printf("%02x ",WAVE[i]);
			//            }
			uart_write_bytes(UART_NUM_1, FORMAT, sizeof(FORMAT));
			//            for(int i = 0;i < sizeof(FORMAT);i++)
			//            {
			//            	printf("%02x ",FORMAT[i]);
			//            }
			uart_write_bytes(UART_NUM_1, WAVE_DATA, sizeof(WAVE_DATA));
			//            for(int i = 0;i < sizeof(WAVE_DATA);i++)
			//            {
			//            	printf("%02x ",WAVE_DATA[i]);
			//            }
			big32little(flash_wr_size, out_little);
			uart_write_bytes(UART_NUM_1, out_little, sizeof(out_little));
			//            for(int i = 0;i < sizeof(out_little);i++)
			//            {
			//            	printf("%02x ",out_little[i]);
			//            }

			uart_write_bytes(UART_NUM_1, flash_read_uart, flash_wr_size);
			//        	for(int i = 0; i < flash_wr_size; i++)
			//        	{
			//        		printf("%02x ",flash_read_uart[i]);
			//        	}
			uart_write_bytes(UART_NUM_1, START_END, sizeof(START_END));

			free(flash_read_uart);

			printf("\n\n}\n\n");
			flash_wr_size = 0;
			ESP_ERROR_CHECK(esp_partition_erase_range(data_partition, 0, RECORD_PARTITION_ERASE_SIZE));
			printf("Erase size: %d Bytes\n", RECORD_PARTITION_ERASE_SIZE);
		}
#endif
	}
}
