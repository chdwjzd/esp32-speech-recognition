#include "user_uart.h"
#include "includes.h"

#include "ota.h"
#include "play_voice.h"
#include "user_spi.h"
#include "PANEL_sdk.h"
#include "sysmsg.h"

#define PATTERN_CHR_NUM    (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
static QueueHandle_t uart1_queue;
uart_event_t event;
uart_event_t event_data;

MSG_T uart1_msg;
ota_cache_msg_T ota_data_cache;
//static char start_OTA[] = {"start OTA"};
//static char zxcg_msg[] = {"zxcg"};			//执行成功
//static char zxsb_msg[] = {"zxsb"};			//执行失败
//static char sblx_msg[] = {"sblx"};			//设备离线
//static char receive_spi[] = {"receive_spi"};
static char pkt_head[] = {"mintPANEL"};
//static char play_music[] = {"play"};

//SPI_REC_MSG_T uart1_spi1;
//
u8 GET_OTA_STATE = 0;

bool get_pkg_head = false;
u16 rec_size = 0;
char REC_buf[1024] = {0};
char *pREC_buf = REC_buf;

u64 lastrec_time = 0;



void data_handle(char *msg_data,int msg_size)
{

	if( xSemaphoreTake( ota_ready, 0 ) == pdTRUE ){

    	char *ota_cache = (char*) malloc(3 * 1024 * 1024 + 256 * 1024);
    	int ota_offest = 0;
    	int stop_ota_count = 0;
    	uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE * 2);

    	memcpy(ota_cache + ota_offest, msg_data, msg_size);
    	ota_offest += msg_size;

		while(1){
//    		ESP_LOGI(TAG, "run OTA");
			if(xQueueReceive(uart1_queue, (void * )&event_data, pdMS_TO_TICKS(10)))
			{

				uart1_msg.rec_size = event_data.size;
				uart_read_bytes(EX_UART_NUM, dtmp, event_data.size, portMAX_DELAY);
				ESP_LOGI(TAG, "get ota data: %d,total get: %d",uart1_msg.rec_size,uart1_msg.rec_size+ota_offest);
			    memcpy(ota_cache + ota_offest, dtmp, event_data.size);
			    ota_offest += event_data.size;
			    stop_ota_count = 0;
			 }
			 else{
			    stop_ota_count++;
			    if(stop_ota_count > 20000){
			    	stop_ota_count = 200;
			    	ESP_LOGI(TAG, "stop_ota_count: %d",stop_ota_count);
			    }
			 }
			 if(stop_ota_count == 100 && ota_offest != 0){
			    ESP_LOGI(TAG, "rec ota data: %d",ota_offest);
			    ota_data_cache.buf = ota_cache;
			    ota_data_cache.cache_size = ota_offest;
			    xQueueSend(ota_data,( void * ) &ota_data_cache,10);
			 }
			 if(xSemaphoreTake(end_ota, 0)){
			    ESP_LOGI(TAG, "stop_ota_count: %d",stop_ota_count);
			    GET_OTA_STATE = 1;
			    break;
			}
		}
	}


	if(xTaskGetTickCount() - lastrec_time > 10 && get_pkg_head == true){
		printf("last tims rec outtime\n");
		get_pkg_head = false;
		rec_size = 0;
//		get_pkg_head = false;
	}
    if(get_pkg_head == false){
    	if(memcmp(pkt_head,msg_data,strlen(pkt_head)) == 0){
    		get_pkg_head = true;
    		bzero(pREC_buf, sizeof(REC_buf));
    	}
        else{
        	if(GET_OTA_STATE == 0){
        		ESP_LOGI(TAG, "Reciver pkt_head err");
        	}
        	else{
        		GET_OTA_STATE = 0;
        	}
        }
    }

    if(get_pkg_head == true){
    	lastrec_time = xTaskGetTickCount();
    	memcpy(pREC_buf + rec_size, msg_data, msg_size);
    	rec_size += msg_size;
    	printf("rec size: %d	address: %p	rectime: %lld\n", msg_size, pREC_buf + rec_size, lastrec_time);
    }
	if(rec_size >= sizeof(PANEL_pkt)){
//    		PANEL_pkt Rec_sdk;
		pPANEL_pkt pRec_sdk = malloc(sizeof(PANEL_pkt));

    	char *Char_pRec_sdk = (char *)pRec_sdk;

		memcpy(Char_pRec_sdk,pREC_buf,sizeof(PANEL_pkt));

//		printf("\n");
//		for(int i = 0; i < sizeof(PANEL_pkt); i++){
//			printf("%02x",Char_pRec_sdk[i]);
//		}
//		printf("\n");


		if(pRec_sdk->head.sourceid.PANEL_did == setmsg.PANEL_did)
		{
    		u16 sum = 0;
    		for(int i = 0; i < sizeof(PANEL_pkt_head) + data_size; i++){
    			sum += pREC_buf[i];
    		}
    		if(sum == pRec_sdk->allsum){
    			ESP_LOGI(TAG, "Reciver packge size: %d", rec_size);
    			rec_msg_T rec_msg;
    			rec_msg.buf = (char *)pRec_sdk;
    			rec_msg.rec_len = rec_size;
    			xQueueSend(sdk_rec_data,( void * ) &rec_msg,10);
    			rec_size = 0;
    		}
    		else{
    			free(pRec_sdk);
    			rec_size = 0;
    			ESP_LOGI(TAG, "Reciver sum check err,rec check sum: %04x, count sum: %04x", pRec_sdk->allsum, sum);
    		}
		}
		else{
			free(pRec_sdk);
			rec_size = 0;
			ESP_LOGI(TAG, "Reciver PANEL_did err: %08x", pRec_sdk->head.sourceid.PANEL_did);
		}
		get_pkg_head = false;
	}
	else{
//		ESP_LOGI(TAG, "Reciver size err");
//		get_pkg_head = false;
	}
}


	/*
//    uart_write_bytes(EX_UART_NUM, test_msg.rec_buffer, test_msg.rec_size);
    if(memcmp(start_OTA,msg_data,strlen(start_OTA)) == 0)
    {
//		 memcmp(start_data,msg_data,strlen(start_data)) == 0
//    	ESP_LOGI(TAG, "start OTA");
    	GET_OTA_STATE = 1;

//    	vTaskDelay(pdMS_TO_TICKS(2));
    	uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE * 2);
    	ESP_LOGI(TAG, "start OTA,OTA_STATE:	%d",GET_OTA_STATE);
    	xSemaphoreGive(start_ota);
    	char *ota_cache = (char*) malloc(3 * 1024 * 1024 + 256 * 1024);
    	memset(ota_cache, 0, 3 * 1024 * 1024 + 256 * 1024);
    	int ota_offest = 0;
    	int stop_ota_count = 0;
    	while(GET_OTA_STATE)
    	{
//    		ESP_LOGI(TAG, "run OTA");
    		if(xQueueReceive(uart1_queue, (void * )&event_data, pdMS_TO_TICKS(10)))
    		{

			    uart1_msg.rec_size = event_data.size;
			    uart_read_bytes(EX_UART_NUM, dtmp, event_data.size, portMAX_DELAY);
			    ESP_LOGI(TAG, "get ota data: %d,total get: %d",uart1_msg.rec_size,uart1_msg.rec_size+ota_offest);
    			memcpy(ota_cache + ota_offest, dtmp, event_data.size);
    			ota_offest += event_data.size;
//    			 bzero(dtmp, RD_BUF_SIZE);
//    			if(event.type == UART_DATA)
//    			{
////    				ESP_LOGI(TAG, "get OTA data");
//    			    uart1_msg.rec_size = event_data.size;
//    			    uart_read_bytes(EX_UART_NUM, dtmp, event_data.size, portMAX_DELAY);
//    			    memcpy(uart1_msg.rec_buffer, dtmp, uart1_msg.rec_size);
//    			    xQueueSend(uart_rec,( void * ) &uart1_msg,10);
////    			    uart_write_bytes(EX_UART_NUM, uart1_msg.rec_buffer, uart1_msg.rec_size);
//    			}
    			stop_ota_count = 0;
    		}
    		else
    		{
    			stop_ota_count++;
    			if(stop_ota_count > 2000)
    			{
    				stop_ota_count = 101;
    				ESP_LOGI(TAG, "stop_ota_count: %d",stop_ota_count);
    			}
    		}
    		if(stop_ota_count == 100 && ota_offest != 0)
    		{
    			ESP_LOGI(TAG, "rec ota data: %d",ota_offest);
    			ota_data_cache.buf = ota_cache;
    			ota_data_cache.cache_size = ota_offest;
    			xQueueSend(ota_data,( void * ) &ota_data_cache,10);
    		}
    		if(xSemaphoreTake(end_ota, 0))
    		{
    			ESP_LOGI(TAG, "stop_ota_count: %d",stop_ota_count);
    			GET_OTA_STATE = 1;
    			break;
    		}
    	}
    	ESP_LOGI(TAG, "UART exit OTA");
    }

    else if(memcmp(zxcg_msg,msg_data,strlen(zxcg_msg)) == 0){
    	int play_num = zxcg;
    	xQueueSend(play_voice_task,( void * ) &play_num,0);
    }

    else if(memcmp(zxsb_msg,msg_data,strlen(zxsb_msg)) == 0){
    	int play_num = zxsb;
    	xQueueSend(play_voice_task,( void * ) &play_num,0);
    }

    else if(memcmp(sblx_msg,msg_data,strlen(sblx_msg)) == 0){
    	int play_num = zxsbqjc;
    	xQueueSend(play_voice_task,( void * ) &play_num,0);
    }

    else if(memcmp(receive_spi,msg_data,strlen(receive_spi)) == 0){
    	char buffer0[20];
    	int sendtask;
    	int rec_len = 0;
    	sscanf(msg_data, "%s %d %d", buffer0, &sendtask, &rec_len);
    	if(rec_len >= 2147483647){
    		printf("rec len too long\r\n");
    	}
    	else
    	{
    		uart1_spi1.rec_size = rec_len;
//    		if(memcmp(play_music,buffer1,strlen(play_music)) == 0)
//    		{
//    			uart1_spi1.rec_task = 1;
//    		}
    		printf("send_task: %d; rec_len: %d\r\n",sendtask,rec_len);
    		xQueueSend(spi_rec,( void * ) &uart1_spi1,0);
//    		printf("rec_len: %d\r\n", rec_len);
    	}
    }	*/

//}




void uart_event_task(void *pvParameters)
{
    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(EX_UART_NUM, &uart_config);

    //Set UART pins (using UART0 default pins ie no changes.)
    uart_set_pin(EX_UART_NUM, 4, 2, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    //Install UART driver, and get the queue.
    uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart1_queue, 0);

    //Set uart pattern detect function.
//    uart_enable_pattern_det_intr(EX_UART_NUM, '+', PATTERN_CHR_NUM, 10000, 10, 10);
    //Reset the pattern queue length to record at most 20 pattern positions.
 //   uart_pattern_queue_reset(EX_UART_NUM, 20);

    //Create a task to handler UART event from ISR

    size_t buffered_size;
    uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE * 2);
    while(1) {
        //Waiting for UART event.
        if(xQueueReceive(uart1_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
            bzero(dtmp, RD_BUF_SIZE);
            ESP_LOGI(TAG, "uart[%d] event:", EX_UART_NUM);
            switch(event.type) {
                //Event of UART receving data
                /*We'd better handler data event fast, there would be much more data events than
                other types of events. If we take too much time on data event, the queue might
                be full.*/
                case UART_DATA:
                    ESP_LOGI(TAG, "[UART DATA]: %d", event.size);
                    uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);

                    ESP_LOGI(TAG, "[DATA EVT]:%s",dtmp);
                    data_handle((char *)dtmp,event.size);
//                    uart_write_bytes(EX_UART_NUM, uart1_msg.rec_buffer, uart1_msg.rec_size);
                    break;
                //Event of HW FIFO overflow detected
                case UART_FIFO_OVF:
                    ESP_LOGI(TAG, "hw fifo overflow");
                    // If fifo overflow happened, you should consider adding flow control for your application.
                    // The ISR has already reset the rx FIFO,
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart1_queue);
                    break;
                //Event of UART ring buffer full
                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG, "ring buffer full");
                    // If buffer full happened, you should consider encreasing your buffer size
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart1_queue);
                    break;
                //Event of UART RX break detected
                case UART_BREAK:
                    ESP_LOGI(TAG, "uart rx break");
                    break;
                //Event of UART parity check error
                case UART_PARITY_ERR:
                    ESP_LOGI(TAG, "uart parity error");
                    break;
                //Event of UART frame error
                case UART_FRAME_ERR:
                    ESP_LOGI(TAG, "uart frame error");
                    break;
                //UART_PATTERN_DET
                case UART_PATTERN_DET:
                    uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
                    int pos = uart_pattern_pop_pos(EX_UART_NUM);
                    ESP_LOGI(TAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                    if (pos == -1) {
                        // There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
                        // record the position. We should set a larger queue size.
                        // As an example, we directly flush the rx buffer here.
                        uart_flush_input(EX_UART_NUM);
                    } else {
                        uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
                        uint8_t pat[PATTERN_CHR_NUM + 1];
                        memset(pat, 0, sizeof(pat));
                        uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
                        ESP_LOGI(TAG, "read data: %s", dtmp);
                        ESP_LOGI(TAG, "read pat : %s", pat);
                    }
                    break;
                //Others
                default:
                    ESP_LOGI(TAG, "uart event type: %d", event.type);
                    break;
            }
        }
    }
}
