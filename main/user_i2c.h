#ifndef _USER_I2C_H_
#define _USER_I2C_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "board.h"


esp_err_t i2c_master_read_slave(uint8_t Address);
esp_err_t i2c_master_write_slave(uint8_t Address,uint8_t data);


#endif
