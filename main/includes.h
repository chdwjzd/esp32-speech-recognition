#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_mem.h"
#include "audio_common.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "esp_audio.h"
#include "esp_wn_iface.h"
#include "esp_wn_models.h"
#include "esp_mn_iface.h"
#include "esp_mn_models.h"
#include "filter_resample.h"
#include "board.h"
#include "rec_eng_helper.h"
#include "esp_partition.h"
#include "driver/uart.h"


#include "soc/efuse_reg.h"
#include "esp_efuse.h"
#include "esp_efuse_table.h"


//static const char *TAG = "P1 TEST";

extern char *TAG;

#define vol 80

#define Hardware_version 			1

#define EX_UART_NUM 				UART_NUM_1

#define show_state        			1


#define OUT_RECORD_UART        		0
#define OUT_VOICE_SPI	        	0

#define FLASH_SECTOR_SIZE			(0x1000)

#define SYS_PARTITION_NAME   		"SYSMSG"
#define SYS_PARTITION_SIZE			0X1000
#define SYS_PARTITION_ERASE_SIZE	(SYS_PARTITION_SIZE % FLASH_SECTOR_SIZE == 0) ? SYS_PARTITION_SIZE : RECORD_PARTITION_SIZE + (FLASH_SECTOR_SIZE - SYS_PARTITION_SIZE % FLASH_SECTOR_SIZE)

#define SET_PARTITION_NAME   		"SETMSG"
#define SET_PARTITION_SIZE			0X1000
#define SET_PARTITION_ERASE_SIZE	(SET_PARTITION_SIZE % FLASH_SECTOR_SIZE == 0) ? SET_PARTITION_SIZE : RECORD_PARTITION_SIZE + (FLASH_SECTOR_SIZE - SET_PARTITION_SIZE % FLASH_SECTOR_SIZE)


#define SHAVALUE_PARTITION_NAME   		"SHAVALUE"
#define SHAVALUE_PARTITION_SIZE			0X1000
#define SHAVALUE_PARTITION_ERASE_SIZE	(SHAVALUE_PARTITION_SIZE % FLASH_SECTOR_SIZE == 0) ? SHAVALUE_PARTITION_SIZE : RECORD_PARTITION_SIZE + (FLASH_SECTOR_SIZE - SHAVALUE_PARTITION_SIZE % FLASH_SECTOR_SIZE)


#if OUT_VOICE_SPI
#define out_voice_maxsize			((int)1 * 16000 * 16 / 8 * 12)		//单声道，16000HZ，16位，8位一字节，最大缓冲12秒
#endif

#if OUT_RECORD_UART
#define RECORD_IN_FLASH_EN        	1
#define RECORD_PARTITION_NAME		"RECORD"
#define RECORD_PARTITION_SIZE		((int)1 * 16000 * 16 / 8 * 12)
#define RECORD_PARTITION_ERASE_SIZE          (RECORD_PARTITION_SIZE % FLASH_SECTOR_SIZE == 0) ? RECORD_PARTITION_SIZE : RECORD_PARTITION_SIZE + (FLASH_SECTOR_SIZE - RECORD_PARTITION_SIZE % FLASH_SECTOR_SIZE)

#else
#define RECORD_IN_FLASH_EN        	0
#endif

//flash record size, for recording 12 seconds' data

#endif
