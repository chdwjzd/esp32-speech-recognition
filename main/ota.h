#ifndef _OTA_H_
#define _OTA_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "board.h"

QueueHandle_t boot_success;

QueueHandle_t start_ota;
QueueHandle_t ota_ready;
QueueHandle_t end_ota;
QueueHandle_t ota_data;

typedef struct ota_cache_msg
{
	char *buf;
	int cache_size;
}ota_cache_msg_T;


TaskHandle_t otaTASK_Handler;
#define OTA_TASK_PRIO		6

void print_sha256 (const uint8_t *image_hash, const char *label);
void ota_task(void *arg);

#endif
