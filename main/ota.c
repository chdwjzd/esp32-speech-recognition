#include "esp_flash_partitions.h"
#include "esp_partition.h"
#include "esp_ota_ops.h"

#include "ota.h"
#include "includes.h"
#include "user_uart.h"

#include "asr.h"
#include "sync_stats.h"
#include "PANEL_sdk.h"
#include "sysmsg.h"

#define OTA_DEBUG 0

ota_cache_msg_T ota_msg;

void print_sha256(const uint8_t *image_hash, const char *label)
{
    char hash_print[32 * 2 + 1];
    hash_print[32 * 2] = 0;
    for (int i = 0; i < 32; ++i)
    {
        sprintf(&hash_print[i * 2], "%02x", image_hash[i]);
    }
    ESP_LOGI(TAG, "%s: %s", label, hash_print);
}

void ota_task(void *arg)
{

    int OTA_STATE = 1;

    uint8_t sha_256[32] = {0};
    esp_partition_t partition;

    // get sha256 digest for the partition table
    partition.address = ESP_PARTITION_TABLE_OFFSET;
    partition.size = ESP_PARTITION_TABLE_MAX_LEN;
    partition.type = ESP_PARTITION_TYPE_DATA;
    esp_partition_get_sha256(&partition, sha_256);
    print_sha256(sha_256, "SHA-256 for the partition table: ");

    // get sha256 digest for bootloader
    partition.address = ESP_BOOTLOADER_OFFSET;
    partition.size = ESP_PARTITION_TABLE_OFFSET;
    partition.type = ESP_PARTITION_TYPE_APP;
    esp_partition_get_sha256(&partition, sha_256);
    print_sha256(sha_256, "SHA-256 for bootloader: ");

    // get sha256 digest for running partition
    esp_partition_get_sha256(esp_ota_get_running_partition(), sha_256);
    print_sha256(sha_256, "SHA-256 for current firmware: ");

    xSemaphoreTake(boot_success, portMAX_DELAY);
    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_ota_img_states_t running_ota_state;
    //
    ESP_LOGI(TAG, "boot success");
    if (esp_ota_get_state_partition(running, &running_ota_state) == ESP_OK)
    {
        if (running_ota_state == ESP_OTA_IMG_PENDING_VERIFY)
        {
            ESP_LOGI(TAG, "cancel rollback");
            esp_ota_mark_app_valid_cancel_rollback();
        }
    }
    PANEL_StartOTA OTA_MSG;
RESTART_OTA:
    xQueueReceive(start_ota, (void *)&OTA_MSG, (portTickType)portMAX_DELAY);

    ESP_LOGI(TAG, "OTA_MSG: StartOTA: %d; OTA_Len: %d; Sendway: %d", OTA_MSG.StartOTA, OTA_MSG.OTA_Len, OTA_MSG.Sendway);

    if (OTA_MSG.StartOTA == false)
    {
        ESP_LOGI(TAG, "STOP OTA example...");
        goto RESTART_OTA;
    }

    esp_err_t err;
    /* update handle : set by esp_ota_begin(), must be freed via esp_ota_end() */
    esp_ota_handle_t update_handle = 0;
    const esp_partition_t *update_partition = NULL;

    ESP_LOGI(TAG, "Starting OTA example...");

    const esp_partition_t *configured = esp_ota_get_boot_partition();
    //    const esp_partition_t *running = esp_ota_get_running_partition();

    if (configured != running)
    {
        ESP_LOGW(TAG, "Configured OTA boot partition at offset 0x%08x, but running from offset 0x%08x",
                 configured->address, running->address);
        ESP_LOGW(TAG, "(This can happen if either the OTA boot data or preferred boot image become corrupted somehow.)");
    }
    ESP_LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             running->type, running->subtype, running->address);

    update_partition = esp_ota_get_next_update_partition(NULL);
    ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x",
             update_partition->subtype, update_partition->address);
    assert(update_partition != NULL);

    vTaskSuspend(asrTASK_Handler);
    vTaskSuspend(StatsTask_Handler);
    ESP_LOGI(TAG, "STOP ASR & Stats show");

    xSemaphoreGive(ota_ready);

    vTaskDelay(pdMS_TO_TICKS(10));

    PANEL_StartOTA Report_OTA;
    pPANEL_StartOTA pReport_OTA = &Report_OTA;

    Report_OTA.StartOTA = true;
    Report_OTA.OTA_Len = 0x00;
    Report_OTA.Sendway = 0x00;

    Send_msg_T SendMSG;
    char *report_OTA_buf = malloc(sizeof(PANEL_StartOTA));
    memcpy(report_OTA_buf, pReport_OTA, sizeof(PANEL_StartOTA));
    SendMSG.send_task = 6;
    SendMSG.buf = report_OTA_buf;
    SendMSG.sendlen = sizeof(PANEL_StartOTA);
    xQueueSend(sdk_send_data, (void *)&SendMSG, 10);

    int localstate = 0;
    localstate = OTA_Mode;
    xQueueOverwrite(LocalState_data, (void *)&localstate); //覆盖写入,输出最新状态
                                                           //配置准备OTA

    //	uart_write_bytes(EX_UART_NUM, "start ota\r\n", 11);
    //等待开始接收OTA
    if (OTA_STATE == 1)
    {

        xQueueReceive(ota_data, (void *)&ota_msg, portMAX_DELAY);
        ESP_LOGI(TAG, "get OTA data");
        esp_app_desc_t new_app_info;
        memcpy(&new_app_info, &ota_msg.buf[sizeof(esp_image_header_t) + sizeof(esp_image_segment_header_t)], sizeof(esp_app_desc_t));
        //        ESP_LOGI(TAG, "New firmware app_elf_sha256: %s", new_app_info.app_elf_sha256);
        print_sha256(new_app_info.app_elf_sha256, "New firmware app_elf_sha256: ");

        esp_app_desc_t running_app_info;
        if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
        {
            ESP_LOGI(TAG, "Running firmware version: %s", running_app_info.version);
        }

        const esp_partition_t *last_invalid_app = esp_ota_get_last_invalid_partition();
        esp_app_desc_t invalid_app_info;
        if (esp_ota_get_partition_description(last_invalid_app, &invalid_app_info) == ESP_OK)
        {
            ESP_LOGI(TAG, "Last invalid firmware version: %s", invalid_app_info.version);
        }

        err = esp_ota_begin(update_partition, 0x350000, &update_handle);
        if (err != ESP_OK)
        {
            OTA_STATE = -1;
            ESP_LOGE(TAG, "esp_ota_begin failed (%s)", esp_err_to_name(err));
            goto OTA_END;
        }
        ESP_LOGI(TAG, "esp_ota_begin succeeded");

        err = esp_ota_write(update_handle, (const void *)ota_msg.buf, ota_msg.cache_size);
        if (err != ESP_OK)
        {
            OTA_STATE = -2;
            ESP_LOGI(TAG, "esp_ota_write failed");
            goto OTA_END;
        }

        ESP_LOGI(TAG, "Total Write binary data length: %d", ota_msg.cache_size);

        err = esp_ota_end(update_handle);

        if (err != ESP_OK)
        {
            OTA_STATE = -3;
            if (err == ESP_ERR_OTA_VALIDATE_FAILED)
            {
                OTA_STATE = -4;
                ESP_LOGE(TAG, "Image validation failed, image is corrupted");
            }
            ESP_LOGE(TAG, "esp_ota_end failed (%s)!", esp_err_to_name(err));
            goto OTA_END;
        }

        ESP_LOGI(TAG, "esp_ota_end success");

        printf("update_partition: %s\r\n", update_partition->label);

        partition.address = update_partition->address;
        partition.size = update_partition->size;
        partition.type = update_partition->type;
        err = esp_partition_get_sha256(&partition, sha_256);
        print_sha256(sha_256, "SHA-256 for update_partition: ");
        if (err != ESP_OK)
        {
            OTA_STATE = -5;
            ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
            goto OTA_END;
        }

        err = esp_ota_set_boot_partition(update_partition);
        if (err != ESP_OK)
        {
            OTA_STATE = -6;
            ESP_LOGE(TAG, "esp_ota_set_boot_partition failed (%s)!", esp_err_to_name(err));
            goto OTA_END;
        }

        xSemaphoreGive(end_ota);

        PANEL_EndOTA END_OTA;
        pPANEL_EndOTA pEND_OTA = &END_OTA;

        END_OTA.OTA_State = 0;
        if (memcmp(update_partition->label, "ota_0", strlen("ota_0")) == 0)
        {
            END_OTA.Next_BOOT_Partition = 0;
        }
        else if (memcmp(update_partition->label, "ota_1", strlen("ota_1")) == 0)
        {
            END_OTA.Next_BOOT_Partition = 1;
        }
        else
        {
            END_OTA.Next_BOOT_Partition = 0xff;
        }
        memcpy(END_OTA.OTA_SHA256, sha_256, sizeof(sha_256));

        char *END_OTA_buf = malloc(sizeof(PANEL_EndOTA));
        memcpy(END_OTA_buf, pEND_OTA, sizeof(PANEL_EndOTA));
        SendMSG.send_task = 7;
        SendMSG.buf = END_OTA_buf;
        SendMSG.sendlen = sizeof(PANEL_EndOTA);
        xQueueSend(sdk_send_data, (void *)&SendMSG, 10);

        ESP_LOGI(TAG, "OTA FINISH");
#if OTA_DEBUG
        uart_write_bytes(EX_UART_NUM, ota_msg.buf, ota_msg.cache_size);
        vTaskDelay(pdMS_TO_TICKS(1000));
#endif
        ESP_LOGI(TAG, "Prepare to restart system!");
        OTA_STATE = 2;
        //        esp_restart();
    }
OTA_END:
    free(ota_msg.buf);
    if (OTA_STATE < 1)
    {
        ESP_LOGI(TAG, "OTA FAILED,END OTA		:%d", OTA_STATE);
    }
    while (1)
    {
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}
