#include "PANEL_sdk.h"
#include "mint_sdk.h"
#include "driver/uart.h"
#include "includes.h"

#include "sysmsg.h"
#include "ota.h"
#include "play_voice.h"
#include "asr.h"

void sendmsg_task(void *arg)
{

	PANEL_pkt Send_sdk;
	pPANEL_pkt pSend_sdk = &Send_sdk;
	char *Char_pSend_sdk = (char *)pSend_sdk;

	memset(pSend_sdk, 0, sizeof(PANEL_pkt));
	memcpy(Send_sdk.head.fixed, "mintPANEL", sizeof(Send_sdk.head.fixed));
	Send_sdk.head.sourceid.PANEL_did = setmsg.PANEL_did;
	memcpy(Send_sdk.head.sourceid.sourceMAC, sysmsg.MAC_addres, sizeof(sysmsg.MAC_addres));
	Send_sdk.head.cmd = 0x00;
	Send_sdk.head.dataLen = 0x00;
	Send_sdk.allsum = 0x00;

	Send_msg_T SendMSG;
	while (1)
	{
		xQueueReceive(sdk_send_data, (void *)&SendMSG, (portTickType)portMAX_DELAY);

		printf("SendMSG.buf: %p\n", SendMSG.buf);

		switch (SendMSG.send_task)
		{
		case 1: //语音识别设备的系统信息
			Send_sdk.head.cmd = MINT_CMD_getdevinfo;
			break;
		case 2: //语音识别设备可配置信息
			Send_sdk.head.cmd = MINT_CMD_get_setinfo;
			break;
		case 3: //回报语音识别结果
			Send_sdk.head.cmd = MINT_CMD_RecognitionMsg;
			break;
		case 4: //采集的语音输出方式
			Send_sdk.head.cmd = MINT_CMD_SendSpeech;
			break;
		case 5: //触发的按键序号
			Send_sdk.head.cmd = MINT_CMD_key;
			break;
		case 6: //是否准备好开始OTA
			Send_sdk.head.cmd = MINT_CMD_update;
			break;
		case 7: //OTA结束回报信息
			Send_sdk.head.cmd = MINT_CMD_updateState;
			break;
		case 8: //获取PANEL功能状态
			Send_sdk.head.cmd = MINT_CMD_FunctionState;
			break;
		default:
			Send_sdk.head.cmd = 0x00;
			break;
		}
		Send_sdk.head.dataLen = SendMSG.sendlen;
		memset(Send_sdk.data, 0, sizeof(Send_sdk.data));
		memcpy(Send_sdk.data, SendMSG.buf, SendMSG.sendlen);
		free(SendMSG.buf);
		//
		u16 sum = 0;
		for (int i = 0; i < sizeof(PANEL_pkt_head) + data_size; i++)
		{
			sum += Char_pSend_sdk[i];
		}
		Send_sdk.allsum = sum;
		printf("SendMSG.allsum: %d  sizeof(Send_sdk): %d\n", Send_sdk.allsum, sizeof(Send_sdk));
		uart_write_bytes(EX_UART_NUM, Char_pSend_sdk, sizeof(Send_sdk));
	}
}

void recmsg_task(void *arg)
{
	PANEL_pkt Rec_sdk;
	pPANEL_pkt pRec_sdk = &Rec_sdk;
	rec_msg_T rec_msg;

	sys_data_msg_T sysmsg_handle;
	set_data_msg_T setmsg_handle;
	PANEL_StartOTA OTAMSG;

	//	PANEL_HandleResult HandleResult;
	pPANEL_HandleResult pHandleResult = malloc(sizeof(PANEL_HandleResult));
	int play_list = -1;

	while (1)
	{
		xQueueReceive(sdk_rec_data, (void *)&rec_msg, (portTickType)portMAX_DELAY);

		memcpy(pRec_sdk, rec_msg.buf, rec_msg.rec_len);
		ESP_LOGI(TAG, "Reciver msg packge; cmd: %d; datalen: %d", Rec_sdk.head.cmd, Rec_sdk.head.dataLen);

		printf("1: %p\r\n", rec_msg.buf);
		for (int i = 0; i < data_size; i++)
		{
			printf("%02x", (unsigned int)Rec_sdk.data[i]);
		}
		printf("\r\n1 end\r\n");

		free(rec_msg.buf);

		switch (Rec_sdk.head.cmd)
		{
		case MINT_CMD_getdevinfo: //读取语音识别设备的系统信息
			sysmsg_handle.readwrite = true;
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_getdevinfo");
			xQueueSend(sys_handle_data, (void *)&sysmsg_handle, 10);
			break;
		case MINT_CMD_setinfo: //设置语音识别设备可配置信息
			setmsg_handle.readwrite = false;
			pPANEL_SetInfo SetInfo_buf = malloc(Rec_sdk.head.dataLen);
			memcpy(SetInfo_buf, Rec_sdk.data, Rec_sdk.head.dataLen);
			setmsg_handle.buf = SetInfo_buf;
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_setinfo");
			xQueueSend(set_handle_data, (void *)&setmsg_handle, 10);
			break;
		case MINT_CMD_get_setinfo: //读取语音识别设备可配置信息
			setmsg_handle.readwrite = true;
			xQueueSend(set_handle_data, (void *)&setmsg_handle, 10);
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_get_setinfo");
			break;
		case MINT_CMD_HandleResult: //通知语音识别执行结果
			memcpy(pHandleResult, Rec_sdk.data, sizeof(PANEL_HandleResult));
			play_list = pHandleResult->PlayNum;
			xQueueSend(play_voice_task, (void *)&play_list, 0);
			if (pHandleResult->Need_SecInterctive == true)
			{
				xSemaphoreGive(SecInterctive);
				ESP_LOGI(TAG, "Reciver msg Need_SecInterctive");
			}
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_HandleResult");
			break;
		case MINT_CMD_update: //准备开始OTA
			OTAMSG.StartOTA = true;
			pPANEL_StartOTA pOTA_MSG = malloc(Rec_sdk.head.dataLen);
			memcpy(pOTA_MSG, Rec_sdk.data, Rec_sdk.head.dataLen);
			OTAMSG.StartOTA = pOTA_MSG->StartOTA;
			OTAMSG.OTA_Len = pOTA_MSG->OTA_Len;
			OTAMSG.Sendway = pOTA_MSG->Sendway;
			free(pOTA_MSG);
			xQueueSend(start_ota, (void *)&OTAMSG, 10);
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_update");
			break;
		case MINT_CMD_reboot: //OTA结束控制重启
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_reboot");
			gpio_set_level(21, 0);
			esp_restart();
			break;
		case MINT_CMD_FunctionState: //获取PANEL功能状态
			functionstate();
			ESP_LOGI(TAG, "Reciver msg MINT_CMD_FunctionState");
			break;
		default:
			ESP_LOGI(TAG, "Reciver unknow packge");
			break;
		}
	}
}