#include "user_spi.h"
#include "includes.h"
#include "math.h"


#define GPIO_MOSI 13
#define GPIO_MISO 12
#define GPIO_SCLK 14
#define GPIO_CS 15
#define SPI_MODE 3

#define SPI_REC_LEN	2048

#define SPI1_DEBUG	1


//char *task2spibuf = NULL;
//char *spi2taskbuf = NULL;



int SPI_SENDING = 0;
int SPI_FREE = 0;

#define bigger2048(a) (a) > (2048) ?  2048 :  a



void spi_task(void *pvParameters)
{
    esp_err_t ret;
    char report[100];
//    SPI_REC_MSG_T spi1_rec_msg;
//    SPI_2_TASK_T spi1_2_task;

    TASK_2_SPI_MSG_T spi1_send_msg;
    spi_slave_transaction_t spislave_send;
//    SPISEND_MSG_T spisend;

    //Configuration for the SPI bus
    spi_bus_config_t buscfg=
    {
        .mosi_io_num=GPIO_MOSI,
        .miso_io_num=GPIO_MISO,
        .sclk_io_num=GPIO_SCLK
    };

    //Configuration for the SPI slave interface
    spi_slave_interface_config_t slvcfg=
    {
        .mode=3,
        .spics_io_num=GPIO_CS,
        .queue_size=3,
        .flags=0//,
    };

    //Enable pull-ups on SPI lines so we don't detect rogue pulses when no master is connected.
    gpio_set_pull_mode(GPIO_MOSI, GPIO_PULLDOWN_ONLY);
    gpio_set_pull_mode(GPIO_SCLK, GPIO_PULLDOWN_ONLY);
    gpio_set_pull_mode(GPIO_CS, GPIO_PULLDOWN_ONLY);

    //Initialize SPI slave interface
    ret=spi_slave_initialize(HSPI_HOST, &buscfg, &slvcfg, 2);
    assert(ret==ESP_OK);

//    spi_send_left = xQueueCreate(20,sizeof(SPISEND_MSG_T));


    char *recbuf = (char *) heap_caps_malloc(SPI_REC_LEN, MALLOC_CAP_DMA);
    char *sendbuf = (char *) heap_caps_malloc(SPI_REC_LEN, MALLOC_CAP_DMA);
//	task2spibuf = (char *) malloc(128 * SPI_REC_LEN);
//	spi2taskbuf = (char *) malloc(128 * SPI_REC_LEN);
//	memset(recbuf, 0, SPI_REC_LEN);
//	memset(sendbuf, 0, SPI_REC_LEN);
//	memset(task2spibuf, 0, 128 * SPI_REC_LEN);
//	memset(spi2taskbuf, 0, 128 * SPI_REC_LEN);
	memset(&spislave_send, 0, sizeof(spislave_send));

    xTaskCreatePinnedToCore(spi_rec_task, "spi_rec_task", 4 * 1024, NULL, SPI_TASK_PRIO, &SPIRecTASK_Handler, 1);

//    spi_slave_transaction_t t;
//    memset(&t, 0, sizeof(t));

	int lastsend_time = 0;
	int nowtime = 0;
	memset(sendbuf, 0, SPI_REC_LEN);

	spislave_send.length = SPI_REC_LEN * 8;
	spislave_send.rx_buffer = recbuf;
	spislave_send.tx_buffer = sendbuf;

    while(1)
    {
    	memset(&spi1_send_msg, 0, sizeof(spi1_send_msg));
    	xQueueReceive(spi_send, ( void * ) &spi1_send_msg, portMAX_DELAY);
    	while(SPI_FREE);
    	SPI_SENDING = 1;
    	int sendoffest = 0;

		//通过串口告诉master要发送多少字节和发送的任务是什么
		memset(&report, 0, sizeof(report));
		sprintf(report, "ESP32 SPI SEND %02d;SIZE:%04d\r\n", spi1_send_msg.send_task, spi1_send_msg.send_size);
		printf("%s",report);

//预防输出太快导致测试时数据丢失
		nowtime = xTaskGetTickCount();
		if(nowtime - lastsend_time < 4){
#if SPI1_DEBUG
    		printf( "vTaskDelay :%d\r\n", 4 - (nowtime - lastsend_time));
#endif
			vTaskDelay(pdMS_TO_TICKS( (4 - (nowtime - lastsend_time))*10));
#if SPI1_DEBUG
    		printf( "Delayfinish\r\n");
#endif
		}
		uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
		lastsend_time = nowtime;

		int total_len = spi1_send_msg.send_size;

    	while(total_len > 0)
    	{
    		int send_len = 0;
    		if(total_len>2048)
    		{
    			send_len = 2048;
    		}
    		else
    		{
    			send_len = total_len;
    		}

    		memcpy(sendbuf, spi1_send_msg.buf + sendoffest, send_len);

    		ret=spi_slave_transmit(HSPI_HOST, &spislave_send, pdMS_TO_TICKS(10));
    		memset(sendbuf, 0, SPI_REC_LEN);
#if SPI1_DEBUG
    		ESP_LOGI(TAG, "send_len:%d, offest:%d, total_len:%d, ret:%x\n", send_len, sendoffest, total_len, ret);
#endif
            sendoffest += send_len;
            total_len -= send_len;
    	}
    	SPI_SENDING = 0;
		free(spi1_send_msg.buf);	//总发送缓冲
    }
}




void spi_rec_task(void *pvParameters)
{
	esp_err_t ret;
	SPI_2_TASK_T spi1_2_task;
	SPI_REC_MSG_T spi1_rec_msg;
	spi_slave_transaction_t spislave_rec;

	char report[100];

	//以下三个malloc不可free
	char *sendbuf = heap_caps_malloc(SPI_REC_LEN,MALLOC_CAP_DMA);
	char *recbuf = heap_caps_malloc(SPI_REC_LEN,MALLOC_CAP_DMA);
	char *spi2task_buf = malloc(256 * SPI_REC_LEN);

	memset(&spislave_rec, 0, sizeof(spislave_rec));
	memset(sendbuf, 0, SPI_REC_LEN);
	memset(spi2task_buf, 0, 256 * SPI_REC_LEN);

	spislave_rec.length = SPI_REC_LEN * 8;
	spislave_rec.rx_buffer = recbuf;
	spislave_rec.tx_buffer = sendbuf;

	while(1)
	{

		memset(&spi1_rec_msg, 0, sizeof(spi1_rec_msg));
		xQueueReceive(spi_rec, ( void * ) &spi1_rec_msg, portMAX_DELAY);
		while(SPI_SENDING);
		SPI_FREE = 1;
    	int total_len = spi1_rec_msg.rec_size;
    	int recoffest = 0;

    	while(total_len > 0)
    	{
    		int rec_len;
    		if(total_len>2048)
    		{
    			rec_len = 2048;
    		}
    		else
    		{
    			rec_len = total_len;
    		}

    		ret=spi_slave_transmit(HSPI_HOST, &spislave_rec, pdMS_TO_TICKS(10));

    		memcpy(spi2task_buf + recoffest, recbuf, rec_len);
    		memset(recbuf, 0, SPI_REC_LEN);
#if SPI1_DEBUG
    		ESP_LOGI(TAG, "send_len:%d, offest:%d, total_len:%d, ret:%x\n", rec_len, recoffest, total_len, ret);
#endif
    		recoffest += rec_len;
            total_len -= rec_len;
    	}
    	SPI_FREE = 0;


#if SPI1_DEBUG
    	vTaskDelay(pdMS_TO_TICKS(2000));
    	printf("\r\n\r\n\r\n");
    		for(int i = 0; i < recoffest; i++){
    			printf("%02x", spi2task_buf[i]);
    		}
    	printf("\r\n\r\n\r\n");
#endif


    	if(spi2task_buf[0] == 0xaa && spi2task_buf[1] == 0xaa ){
    		uint8_t sum = 0;
    //				char *data_buf = (char *) malloc(message_len);
    //				memcpy(data_buf, spi_buf, message_len);
    		for(int i = 0; i < recoffest; i++)
    			sum += *(spi2task_buf + i);

    		if(!(sum == *(spi2task_buf + recoffest - 1))){
    			memset(&report, 0, sizeof(report));
    			sprintf(report, "spi rec checksum failed: %d %d\r\n", sum, *(spi2task_buf + recoffest - 1));
    			uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
    		}
    		else{
           		char *data_buf = (char *) malloc(recoffest - 3);
        		memcpy(data_buf,(char *)(spi2task_buf + 2), recoffest -3);

            	spi1_2_task.buf = data_buf;
            	spi1_2_task.rec_size = recoffest - 3;
            	spi1_2_task.rec_task = spi1_rec_msg.rec_task;
            	if(xQueueSend(spi_2_task, ( void * ) &spi1_2_task, pdMS_TO_TICKS(10)) != pdTRUE){
            		free(data_buf);
            		memset(&report, 0, sizeof(report));
        			sprintf(report, "spi rec send task failed\r\n");
        			uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
            	}
            	else{
            		memset(&report, 0, sizeof(report));
            		sprintf(report, "spi rec success\r\n");
            		uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
            	}
    		}
    	}
    	else{
			memset(&report, 0, sizeof(report));
			sprintf(report, "spi rec head err: %d %d\r\n", recbuf[0], recbuf[1]);
			uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
    	}

	}
}








/*
		if(spi_slave_transmit(HSPI_HOST, &spitransaction, portMAX_DELAY) == ESP_OK){
//			SPI_FREE =1;

			if(SPI_SENDING == 2)SPI_SENDING = 3;
			if(left_send > 0){
				left_send -= bigger2048(left_send);
				memcpy(sendbuf, spisend.buf, bigger2048(left_send));
				if(left_send == 0)SPI_SENDING = 2;
			}
			else{
				if(xQueueReceive(spi_send_left, ( void * ) &spisend, 0) != pdTRUE){
					if(spisend.send_size > SPI_REC_LEN){
						left_send = spisend.send_size - spisend.offest;

					}
					else{
						if(SPI_SENDING == 1){
							SPI_SENDING = 0;
							vTaskDelay(pdMS_TO_TICKS(3));
						}
					}
				}
				else{
					SPI_SENDING = 3;
				}

			}
		}
	}
}




*/



/*

		if(SPI_SENDING == 0)
		{
			if(spi_slave_transmit(HSPI_HOST, &t, pdMS_TO_TICKS(10)) == ESP_OK)
			{
				ESP_LOGI(TAG,"SPI REC");
				if(recbuf[0] == 0xaa && recbuf[1] == 0xaa )
				{
					memset(sendbuf, 0, SPI_REC_LEN);
					memset(recbuf, 0, SPI_REC_LEN);
					memset(spi_buf, 0, 256 * SPI_REC_LEN);
					int spi_time = 0;
					int rec_len = 0;
					int message_len = (recbuf[5]<<24) + (recbuf[4]<<16) + (recbuf[3]<<8) + recbuf[2];
					memcpy(spi_buf + rec_len, recbuf, SPI_REC_LEN);
					rec_len += SPI_REC_LEN;
					while(message_len >= rec_len && spi_time < 5)
					{
						memset(recbuf, 0, SPI_REC_LEN);
						ret = spi_slave_transmit(HSPI_HOST, &t, pdMS_TO_TICKS(10));
						if(ret == ESP_OK)
						{
							memcpy(spi_buf + rec_len, recbuf, SPI_REC_LEN);
							spi_time = 0;
							rec_len += SPI_REC_LEN;
						}
						else
						{
							spi_time++;
						}
					}
					if(spi_time > 5)
					{
						memset(&report, 0, sizeof(report));
		        		sprintf(report, "spi rec out time\r\n");
		        		uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
					}
					else
					{
						uint8_t sum = 0;
		//				char *data_buf = (char *) malloc(message_len);
		//				memcpy(data_buf, spi_buf, message_len);
						for(int i = 0; i < (message_len-1); i++)
							sum += *(spi_buf + i);

		        		if(!(sum == *(spi_buf + message_len - 1)))
		        		{
		        			memset(&report, 0, sizeof(report));
		        			sprintf(report, "spi rec checksum failed: %d %d\r\n", sum, *(spi_buf + message_len - 1));
		        			uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
		        		}
		        		else
		        		{
		               		char *data_buf = (char *) malloc(message_len - 7);
		            		memcpy(data_buf, spi_buf, message_len);

		                	spi1_2_task.buf = data_buf;
		                	spi1_2_task.rec_size = message_len;
		                	spi1_2_task.rec_task = data_buf[6];
		                	if(xQueueSend(spi_2_task, ( void * ) &spi1_2_task, pdMS_TO_TICKS(10)) != pdTRUE)
		                	{
		                		free(data_buf);
		                		memset(&report, 0, sizeof(report));
		            			sprintf(report, "spi rec send task failed\r\n");
		            			uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
		                	}
		                	else
		                	{
		                		memset(&report, 0, sizeof(report));
		                		sprintf(report, "spi rec success\r\n");
		                		uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
		                	}
		        		}
					}
				}
				else
				{
					memset(&report, 0, sizeof(report));
					sprintf(report, "spi rec head err: %d %d\r\n", recbuf[0], recbuf[1]);
					uart_write_bytes(EX_UART_NUM, report, strlen(report) + 1);
				}
			}
		}
		else
		{
			SPI_SENDING = 2;
//			free(sendbuf);
//			free(recbuf);
//			free(spi_buf);

			ESP_LOGI(TAG,"23123123");
			while(SPI_SENDING == 2);
			ESP_LOGI(TAG,"53456");
//			vTaskDelete(SPIRecTASK_Handler);
			ESP_LOGI(TAG,"8678");
		}

	}

}
*/
//Clear receive buffer, set send buffer to something sane
//        memset(recvbuf, 0x00, 2048);
////        sprintf(sendbuf, "%04d.", n);
////        sprintf((char*)sendbuf, "ESP32 SEND, sending data for transmission no. %04d.", n);
//        //Set up a transaction of 128 bytes to send/receive			This is the receiver, sending data for transmission number
//        t.length=1024*8*2;
//        t.trans_len=1024*8;
//        t.tx_buffer=sendbuf;
//        t.rx_buffer=recvbuf;
//        /* This call enables the SPI slave interface to send/receive to the sendbuf and recvbuf. The transaction is
//        initialized by the SPI master, however, so it will not actually happen until the master starts a hardware transaction
//        by pulling CS low and pulsing the clock etc. In this specific example, we use the handshake line, pulled up by the
//        .post_setup_cb callback that is called as soon as a transaction is ready, to let the master know it is free to transfer
//        data.
//        */
//        ret=spi_slave_transmit(HSPI_HOST, &t, portMAX_DELAY);
//
//        //spi_slave_transmit does not return until the master has done a transmission, so by here we have sent our data and
//        //received data from the master. Print it.
//        printf("ESP32 Received: %s; ret: %d; len: %d\n", recvbuf, ret, t.length);
//        n++;

/*
esp_err_t ret;
spi_device_handle_t handle;

spi_bus_config_t buscfg={
    .mosi_io_num=GPIO_MOSI,
    .miso_io_num=GPIO_MISO,
    .sclk_io_num=GPIO_SCLK,
    .quadwp_io_num=-1,
    .quadhd_io_num=-1
};



//Configuration for the SPI device on the other side of the bus
spi_device_interface_config_t devcfg={
    .command_bits=0,
    .address_bits=0,
    .dummy_bits=0,
    .clock_speed_hz=100000,
    .duty_cycle_pos=128,        //50% duty cycle
    .mode=SPI_MODE,
    .spics_io_num = GPIO_CS,
	.cs_ena_pretrans = 3,
    .cs_ena_posttrans = 3,        //Keep the CS low 3 cycles after transaction, to stop slave from missing the last bit when CS has less propagation delay than CLK
    .queue_size=3

};

//    int n=0;
char sendbuf[1024] = {0};
char recvbuf[1024] = {0};
spi_transaction_t t;
memset(&t, 0, sizeof(t));


//Initialize the SPI bus and add the device we want to send stuff to.
ret=spi_bus_initialize(HSPI_HOST, &buscfg, 2);
assert(ret==ESP_OK);
ret=spi_bus_add_device(HSPI_HOST, &devcfg, &handle);
assert(ret==ESP_OK);

int total_len = 0;

char old_task[15] = {0};
while(1) {
	xQueueReceive(spi_rec, ( void * ) &spi1_rec_msg, 30);
//    	printf("send_task: %d	rec_size: %d\r\n",spi1_rec_msg.send_task,spi1_rec_msg.rec_size);
	if(spi1_rec_msg.rec_task != 0)
	{
    	total_len = spi1_rec_msg.rec_size;
    	char *spi_buf;
    	int offest = 0;
    	spi_buf = (char *) malloc(512 * 1024);
    	printf("sizeof(spi_buf) = %d	address %x\n", sizeof(spi_buf), (unsigned int)spi_buf);
    	while(total_len > 0)
    	{
    		int rec_len = 0;
    		if(total_len>1024)
    		{
    			rec_len = 1024;
    		}
    		else
    		{
    			rec_len = total_len;
    		}
    		memset(sendbuf, 0, sizeof(sendbuf));
//    		memset(out_buf, 0, sizeof(sendbuf));
    		memset(recvbuf, 0, sizeof(recvbuf));
    		t.rx_buffer = recvbuf;
    		t.tx_buffer = sendbuf;
    		t.length = 1024 * 8;

            ret=spi_device_transmit(handle, &t);
//            rec_len = t.rxlength;

#if SPI1_DEBUG
            char out_buf[1024] = {0};
            memset(out_buf, 0, sizeof(out_buf));
            memcpy(out_buf, recvbuf, rec_len);
            printf("Received: %s	rec_len:%d	total_len%d	out_buf%d\n", out_buf, rec_len, total_len, strlen(out_buf));
#endif

            memcpy(spi_buf + offest, recvbuf, rec_len);


            total_len = total_len - rec_len;
            rec_len = 0;
            offest += 1024;
    	}

    	spi1_2_task.buf = spi_buf;
    	spi1_2_task.rec_size = spi1_rec_msg.rec_size;
    	spi1_2_task.rec_task = spi1_rec_msg.rec_task;
    	xQueueSend(spi_2_task,( void * ) &spi1_2_task,1);

    	spi1_rec_msg.rec_task = 0;
    	spi1_rec_msg.rec_size = 0;
	}





	xQueueReceive(spi_send, ( void * ) &spi1_send_msg, 30);

	if(spi1_send_msg.send_size != 0)
	{

		int offest = 0;
		int send_len = 0;
		total_len = spi1_send_msg.send_size;

		char *rec_buf;
		rec_buf = (char *) heap_caps_malloc(4096, MALLOC_CAP_DMA);
		t.rx_buffer = rec_buf;

  		char start[] = {"start"};
		char *send_buf;
		send_buf = (char *) heap_caps_malloc(4096, MALLOC_CAP_DMA);


		if(memcmp(old_task,spi1_send_msg.send_task,strlen(spi1_send_msg.send_task) != 0))
		{

			memcpy(old_task, spi1_send_msg.send_task, strlen(spi1_send_msg.send_task));

    		memset(send_buf, 0, 4096);

    		memcpy(send_buf, spi1_send_msg.send_task, strlen(spi1_send_msg.send_task));
    		offest = strlen(spi1_send_msg.send_task);

    		memcpy(send_buf + offest, start, strlen(start));
    		offest = offest + strlen(start);

    		if(spi1_send_msg.send_size <= 4000){
    			send_len = spi1_send_msg.send_size;
    			total_len = 0;
    		}
    		else{
    			send_len = 4000;
    			total_len = spi1_send_msg.send_size - 4000;
    		}

    		memcpy(send_buf + offest, spi1_send_msg.buf, send_len);

    		t.tx_buffer = send_buf;
    		t.length = (send_len + offest) * 8;
    		t.rxlength = 4;

    		ret=spi_device_transmit(handle, &t);

    		if(total_len > 0)
    		{
    			offest = 4000;
    		}
#if SPI1_DEBUG
           printf("send_len:%d\n", send_len + strlen(spi1_send_msg.send_task) + strlen(start));
#endif
		}


		while(total_len > 0)
		{
			memset(rec_buf, 0, 4096);
			memset(send_buf, 0, 4096);
    		if(total_len > 4000)
    		{
    			send_len = 4000;
    		}
    		else
    		{
    			send_len = total_len;
    		}

			memcpy(send_buf, spi1_send_msg.buf + offest, send_len);
			t.rx_buffer = rec_buf;
    		t.tx_buffer = send_buf;
    		t.length = send_len * 8;
    		t.rxlength = 4;

			ret=spi_device_transmit(handle, &t);

			offest += 4000;
			total_len -= send_len;

#if SPI1_DEBUG
            printf("send_len:%d\n", send_len);
#endif

		}
		free(rec_buf);
		free(send_buf);
		free(spi1_send_msg.buf);
		spi1_send_msg.send_size = 0;

		xQueueReceive(spi_rec, ( void * ) &spi1_rec_msg, 0);
		if(spi1_rec_msg.rec_task != 0)
		{
			char spi_rec_failde[] = {"spi rec failde"};
			uart_write_bytes(EX_UART_NUM, spi_rec_failde, strlen(spi_rec_failde) + 1);
		}


	}
}

*/

//    	if(xQueueReceive(spi_send, &(rec_len), 500))
//    	{
//
//    	}
//        int res = snprintf(sendbuf, sizeof(sendbuf),
//                "no.%04i.:%s\n", n, recvbuf);
//        if (res >= sizeof(sendbuf)) {
//            printf("Data truncated\n");		//内容过多数据缩短
//        }
//        memset(recvbuf, 0, sizeof(recvbuf));
//        t.length=sizeof(sendbuf)*8;
//        t.tx_buffer=sendbuf;
//        t.rx_buffer=recvbuf;
//
//        //Wait for slave to be ready for next byte before sending
////        xSemaphoreTake(rdySem, portMAX_DELAY); //Wait until slave is ready
//        ret=spi_device_transmit(handle, &t);
//        rec_len = t.rxlength;
//        printf("Received: %s	rec_len:%d\n", recvbuf,rec_len);
//        n++;
//        vTaskDelay(pdMS_TO_TICKS(1000));
