#ifndef _PLAY_VIOCE_H_
#define _PLAY_VIOCE_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_err.h"

#include "esp_log.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_mem.h"
#include "audio_common.h"
#include "i2s_stream.h"
#include "board.h"



//#define nihao 0			//你好
//#define zai 1			//在
//#define zsh 2			//早上好
//#define haode 3			//好的
//#define zxcg 4			//执行成功
//#define zxsb 5			//执行失败
//#define zxsbqjc 6		//执行失败，请检查后重试
//#define wmtd 7			//我没听懂
//#define wmtqc 8			//我没听清楚，请再说一次





TaskHandle_t PlayTask_Handler;
#define PLAY_TASK_PRIO     	10

audio_board_handle_t voice_control;

QueueHandle_t play_finish;

QueueHandle_t play_voice_task;

void play_task(void *arg);

#endif
