#ifndef _USER_UART_H_
#define _USER_UART_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "driver/uart.h"

#include "esp_log.h"
#include "board.h"


typedef struct MSG
{
	int rec_size;
	char rec_buffer[1024];
}MSG_T;

TaskHandle_t uartTASK_Handler;
#define UART_TASK_PRIO		9

QueueHandle_t uart_rec;

void uart_event_task(void *pvParameters);

#endif
