#ifndef MINT_SDK_H
#define MINT_SDK_H
#include <stdint.h>

#define nullptr NULL
#define MINT_CMD_pkt_Server_data_MAX_size 2048
#define u8 unsigned char

typedef enum
{
    UNKNOW_PROTOCOL = 0,
    GW_PROTOCOL,
    DEV_PROTOCOL,
    RETURN_PROTOCOL,

} MINT_PROTOCOL_TYPE;

typedef enum
{
    MINT_CMD_unknow = 0,
    MINT_CMD_times,
    MINT_CMD_initial,
    MINT_CMD_initialWithBaseroom,
    MINT_CMD_searchDevWithBaseroom,
    MINT_CMD_setscene,
    MINT_CMD_getallstatus,
    MINT_CMD_activation,
    MINT_CMD_postDevInfo,
    MINT_CMD_mintiot_activation,
    MINT_CMD_updata,
    MINT_CMD_update,
    MINT_CMD_reboot,
    MINT_CMD_resetactivation,
    MINT_CMD_batchmatchmode,
    MINT_CMD_templatematchmode,
    MINT_CMD_exittemplatematchmode,
    MINT_CMD_exitbatchmatchmode,
    MINT_CMD_syncdata,
    MINT_CMD_synctotaldata,
    MINT_CMD_matchresult,
    MINT_CMD_matchingfinlish,
    MINT_CMD_blueprintdata,
    MINT_CMD_downloadConfig,
    MINT_CMD_scenes,
    MINT_CMD_getscenesdev,
    MINT_CMD_default_scenes,
    MINT_CMD_matchcontrol,
    MINT_CMD_matchscenefinlish,
    MINT_CMD_matchrefresh,
    MINT_CMD_scenes_trans,
    MINT_CMD_getip,
    MINT_CMD_getstatus,
    MINT_CMD_open,
    MINT_CMD_close,
    MINT_CMD_adjust,
    MINT_CMD_stop,
    MINT_CMD_invert, //取反 yzj20201020
    MINT_CMD_key,
    MINT_CMD_infrared,       //红外工程配置
    MINT_CMD_infrared_Match, //
    MINT_CMD_infrared_learn, //红外learn工程配置

    MINT_CMD_netjoin,     //入网配置
    MINT_CMD_exitnetjoin, //退网配置
    MINT_CMD_ftpenable,   //ftp使能
    MINT_CMD_ftpupload,   //ftp文件上传
    MINT_CMD_click,
    MINT_CMD_online,
    MINT_CMD_mplay,
    MINT_CMD_mstop,
    MINT_CMD_mnext,
    MINT_CMD_mprev,
    MINT_CMD_mvol_up,
    MINT_CMD_mvol_down,
    MINT_CMD_match,
    MINT_CMD_arrayCmd, //群控制指令,理论上最大值支持50个
    MINT_CMD_uploadlog,
    MINT_CMD_ali_initialregister, //请求注册的设备列表
    MINT_CMD_ali_register_result, //注册结果
    MINT_CMD_ali_registerfinlish, //注册结束
    MINT_CMD_ali_activation,      //设备激活
    MINT_CMD_getdevinfo,          //获取设备信息
    MINT_CMD_propertyid_bind,     //房产信息绑定
    MINT_CMD_ali_configdel,       //设备信息删除
    MINT_CMD_returnCode_1,        //执行结果返回
    MINT_CMD_returnCode_2,        //带更新数据
    MINT_CMD_returnCode_6,        //场景列表
    MINT_CMD_returnCode_7,        //场景列表与场景列表的设备
    MINT_CMD_returnCode_8,        //场景模板
    MINT_CMD_returnCode_9,        //场景触发者列表
    //MINT_CMD_uploadlog_local_net

    MINT_CMD_get_setinfo = 0xe000, //获取设置的参数
    MINT_CMD_setinfo,              //设置设备的参数
    MINT_CMD_get_sysinfo,          //获取系统的参数
    MINT_CMD_RecognitionMsg,       //语音识别结果
    MINT_CMD_SendSpeech,           //返回采集到的语音
    MINT_CMD_HandleResult,         //执行的结果
    MINT_CMD_updateState,          //升级的结果
    MINT_CMD_FunctionState,        //功能状态
    MINT_CMD_Scenemode

} MINT_CMD;

typedef enum
{
    MINT_RETURN_unknown,
    MINT_RETURN_execution_failed = false,
    MINT_RETURN_execution_succeed = true,
    MINT_RETURN_dev_State_changes

} MINT_RETURN_RESULT;

typedef enum
{
    MINT_subCMD_unknow = 0,
    MINT_subCMD_addscene,
    MINT_subCMD_bindscenes2exec,
    MINT_subCMD_bindlogi,
    MINT_subCMD_getscenes,
    MINT_subCMD_getscenesdev,
    MINT_subCMD_removescene,
    MINT_subCMD_startscene,
    MINT_subCMD_stopscene,
    MINT_subCMD_clickscene,
    MINT_subCMD_getSceneTemplate,
    MINT_subCMD_callhomescene,
    MINT_subCMD_sethomescene,
    MINT_subCMD_removealllogi,
    MINT_subCMD_removeonelogi,
    MINT_subCMD_getalltrigger,
    MINT_subCMD_getdefaultscenes,
    MINT_subCMD_getcustomscenes,

    MINT_subCMD_write_totalcode,
    MINT_subCMD_write_buttoncode,
    MINT_subCMD_infrared_match,
    MINT_subCMD_infrared_learn,
} MINT_SUBCMD;

typedef enum
{
    MINT_RECVFROM_LOCAL_CLIENT,
    MINT_RECVFROM_MINT_MQTT,
    MINT_RECVFROM_ALIYUN,
    MINT_RECVFROM_VOICE_CONTROL,
} MINT_RECVFROM;

typedef enum
{
    MINT_SCENE_Back_Home = 1,   /*回家*/
    MINT_SCENE_Leave_Home,      /*离家*/
    MINT_SCENE_Get_Up,          /*起床*/
    MINT_SCENE_Get_Up_At_Nig,   /*起夜 */
    MINT_SCENE_Sleep,           /*睡眠*/
    MINT_SCENE_Cook,            /*烹饪*/
    MINT_SCENE_Western_Food,    /*西餐*/
    MINT_SCENE_Chinese_Food,    /*中餐*/
    MINT_SCENE_Dinner_Party,    /*聚餐*/
    MINT_SCENE_Have_Dinner,     /*用餐*/
    MINT_SCENE_Shower,          /*洗澡*/
    MINT_SCENE_Wash,            /*洗漱*/
    MINT_SCENE_Top_Light,       /*顶灯*/
    MINT_SCENE_Belt_Light,      /*灯带*/
    MINT_SCENE_Down_Light,      /*筒灯*/
    MINT_SCENE_Open_Curtains,   /*开窗帘*/
    MINT_SCENE_Stop_Curtains,   /*停窗帘*/
    MINT_SCENE_Close_Curtain,   /*关窗帘*/
    MINT_SCENE_Open_Screen_W,   /*开纱窗*/
    MINT_SCENE_Close_Screen_,   /*关纱窗*/
    MINT_SCENE_Entertainment,   /*娱乐*/
    MINT_SCENE_Read,            /*阅读*/
    MINT_SCENE_Rest,            /*休息*/
    MINT_SCENE_Work,            /*办公*/
    MINT_SCENE_Speech,          /*演讲*/
    MINT_SCENE_Television,      /*电视*/
    MINT_SCENE_Projection,      /*投影*/
    MINT_SCENE_Video,           /*影音*/
    MINT_SCENE_Meeting,         /*会客*/
    MINT_SCENE_Full_Open,       /*全开*/
    MINT_SCENE_Full_Close,      /*全关*/
    MINT_SCENE_Go_To_Work,      /*上班*/
    MINT_SCENE_Go_Off_Work,     /*下班*/
    MINT_SCENE_Double_Control,  /*双控*/
    MINT_SCENE_RIVER_VIEW,      /*江景*/
    MINT_SCENE_LEAVE_HOME_LONG, /*长期离家*/
    MINT_SCENE_OLD_MAN,         /*老人*/
    MINT_SCENE_CHILD,           /*儿童*/
    //MINT_SCENE_Double_Control,    /*双控*/
    MINT_SCENE_Unknow, /*未知*/

    MINT_SCENE_COUNT, /*模板总数*/

} MINT_SCENE_TEMPLATE;

typedef struct
{
    u8 roomid[2]; //用short型，字节会以大端存储，可能会影响转义
    u8 did;
    u8 nid; //协调器
    u8 dtype;
    u8 subtype;
    u8 cid;
    u8 reserve0; //显示表示字节对齐的隐藏空间
} MINT_CCMDID;

typedef struct
{
    u8 pib[8];
    u8 random[2];
} MINT_PIB;

typedef struct
{
    u8 logiid;
    //MINT_SCENE_TRIG_TYPE triggerType;
    MINT_SCENE_TEMPLATE sceneTemplate_id;

} MINT_LOGI_MODEL;

typedef struct
{
    MINT_CMD cmd;
    MINT_SUBCMD subCmd;
    MINT_CCMDID ccmdid;
} MINT_GW_PROTOCOL; //网关级命令

typedef struct
{
    bool getstatus;
    bool open;
    bool close;
    bool stop;
    bool adjust;
    bool key;
    bool click;
} MINT_dev_fuction;

typedef struct
{
    MINT_CMD cmd;
    MINT_CCMDID ccmid;
} MINT_DEV_PROTOCOL_Server; //名特协议服务端用的

typedef struct
{
    MINT_CMD cmd;
    MINT_RETURN_RESULT result;

} MINT_CMD_RETURN;

typedef struct
{
    MINT_CMD cmd;
    MINT_LOGI_MODEL logi_model;
} MINT_SCENE_PROTOCOL;

// typedef struct
// {
//     MINT_SCENE_TEMPLATE MINT_SCENE_EXE_ID;
//     MINT_SCENE_TRIG_TYPE scene_trigger_type;
//     u8 times;
// }MINT_SCENE_EXE;

/*
typedef struct
{
    MINT_SCENE_TRIG_TYPE trigger_type;
    CMD_Adjust_Data exec_data;
}CMD_SCENE_Data;
*/

typedef struct
{
    u8 ccmid[32];
    u8 did[32];
    u8 cname[32];
    u8 dname[32];
    u8 rname[32];
    u8 dtype[10];
    u8 subtype[10];
    MINT_dev_fuction fuction;
} MST_dev_info;

typedef struct
{
    MINT_CMD cmd;
    u8 dtype[10];
    u8 subtype[10];
    u8 did[32];
    u8 ccmid[32];
    u8 sid[10];
    MINT_dev_fuction fuction;

} MINT_DEV_PROTOCOL;

typedef struct
{
    u8 recvFrom;
    u8 sourceId;
    u8 sourceNet;
    u8 sourceType;
} MINT_SOURCEID;

typedef enum
{
    MINT_INFRARED_unknow = 0,
    MINT_INFRARED_totalcode,
    MINT_INFRARED_buttoncode,

} MINT_INFRARED_codetype;

typedef struct
{
    u8 codetype;
    u8 bid;
    u8 btype[20];
    u8 btypeid[10];
    MINT_CCMDID ccmid;
    u8 bname[30];
    u8 buttonid; //yzj 20200806
    u8 bpath[100];
} MINT_INFRARED;

typedef struct
{
    u8 md5_sum[64];
    u8 ftp_file_path[64];
} MINT_FTP_TRANSFER;

typedef struct
{
    char fixed[4]; //默认头 'm' 'i' 'n' 't' ,初始化的时候需要自己赋予
    int_least64_t phoneNum;
    int_least64_t Sn;
    MINT_SOURCEID sourceid;
    MINT_PIB pib;
    MINT_PROTOCOL_TYPE type;
    union
    {
        MINT_GW_PROTOCOL gw;
        MINT_DEV_PROTOCOL_Server dev;
        MINT_SCENE_PROTOCOL scene;
        MINT_CMD_RETURN ret;
    } MINT_PROTOCOL_DATA;
    u8 totalPage;
    u8 PageNo;
    short dataLen; //data的长度
} MINT_CMD_pkt_Server_head;

// #define MINT_CMD_pkt_Server_data_MAX_size (((sizeof (MINT_CMD_pkt_Server_head)+sizeof (CMD_Adjust_Data))*MINT_MAX_COMMAND_SIZE) < 2048 ? 2048 :2048)

typedef struct
{
    MINT_CMD_pkt_Server_head head;
    u8 data[MINT_CMD_pkt_Server_data_MAX_size];
} MINT_CMD_pkt_Server;

// typedef struct
// {
//     int_least64_t phoneNum;
//     int_least64_t Sn;
//     MINT_SOURCEID sourceid;
//     MINT_PROTOCOL_TYPE type;
//     union
//     {
//         MINT_GW_PROTOCOL gw;
//         MINT_DEV_PROTOCOL dev;
//     };
//     int returnTimes;
//     int returnCode[RETURN_CODE_MAX_TIMES];
//     int dataLen;
// }MINT_CMD_pkt;

#endif // MINT_SDK_H
