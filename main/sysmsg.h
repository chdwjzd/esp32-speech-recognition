#ifndef _SYSMSG_H_
#define _SYSMSG_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "esp_partition.h"

#include "esp_log.h"
#include "board.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "PANEL_sdk.h"

PANEL_SYSInfo sysmsg;
PANEL_SetInfo setmsg;

QueueHandle_t sys_handle_data;
QueueHandle_t set_handle_data;
QueueHandle_t LocalState_data;

typedef struct sys_data_msg
{
	bool readwrite;				//True:read; Flase:write
}sys_data_msg_T;

typedef struct set_data_msg
{
	bool readwrite;				//True:read; False:write
	PANEL_SetInfo *buf;
}set_data_msg_T;

#define sysmsg_handle_PRIO		4
#define setmsg_handle_PRIO		3

TaskHandle_t sysmsg_task_Handler;
TaskHandle_t setmsg_task_Handler;



void updata_sha256(const esp_partition_t *updata_sha256_partition);
bool check_sha256(const esp_partition_t *cheak_sha256_partition);
void msg_init(void);
void sysmsg_handle_task(void *arg);
void setmsg_handle_task(void *arg);
void functionstate(void);

#endif
