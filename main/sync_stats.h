#ifndef _SYNC_STATS_H_
#define _SYNC_STATS_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"

#include "esp_log.h"
#include "board.h"


#define STATS_TICKS         pdMS_TO_TICKS(1000)
#define ARRAY_SIZE_OFFSET   5

SemaphoreHandle_t sync_stats_task;

TaskHandle_t StatsTask_Handler;
#define STATS_TASK_PRIO     8

void stats_task(void *arg);

#endif
