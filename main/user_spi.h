#ifndef _USER_SPI_H_
#define _USER_SPI_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"


#include "driver/spi_master.h"

#include "esp_log.h"
#include "board.h"

QueueHandle_t spi_rec;
QueueHandle_t spi_send;
QueueHandle_t spi_2_task;
QueueHandle_t spi_send_left;

TaskHandle_t SPITASK_Handler;
TaskHandle_t SPIRecTASK_Handler;
#define SPI_TASK_PRIO		7

typedef struct SPI_REC_MSG
{
	int rec_size;
	int rec_task;
}SPI_REC_MSG_T;

typedef struct SPI_2_TASK_MSG
{
	char *buf;
	int rec_size;
	int rec_task;
}SPI_2_TASK_T;

typedef struct TASK_2_SPI_MSG
{
	char *buf;
	int send_size;
	int send_task;
}TASK_2_SPI_MSG_T;

typedef struct SPISEND_MSG
{
	char *buf;
	int send_size;
	int offest;
}SPISEND_MSG_T;

void spi_task(void *pvParameters);
void spi_rec_task(void *pvParameters);

#endif
