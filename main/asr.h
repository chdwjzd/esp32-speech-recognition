#ifndef _ASR_H_
#define _ASR_H_

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "esp_err.h"

#include "esp_log.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_mem.h"
#include "audio_common.h"
#include "i2s_stream.h"
#include "raw_stream.h"
#include "esp_audio.h"
#include "esp_wn_iface.h"
#include "esp_wn_models.h"
#include "esp_mn_iface.h"
#include "esp_mn_models.h"
#include "filter_resample.h"
#include "board.h"
#include "rec_eng_helper.h"
#include "esp_partition.h"
#include "esp_vad.h"




QueueHandle_t asrinit_finish;
QueueHandle_t SecInterctive;

typedef enum {
    WAKE_UP = 1,
} asr_wakenet_event_t;





TaskHandle_t asrTASK_Handler;
#define SPEECH_TASK_PRIO	11

void speech_task(void *arg);
esp_err_t asr_multinet_control(int commit_id);


#define sleep			0
#define wakeup			1
#define asr_multinet	2
#define multinet_succes	3
#define multinet_faild	4



QueueHandle_t asr_state;

#endif

